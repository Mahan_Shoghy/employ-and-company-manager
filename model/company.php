<?php
    # return all detail of one company 
    function AllFromCompany($email)
    {
        return mysqli_fetch_all(querySQL("SELECT idusers , company_name , field_name , idcompany from users
        inner join company on idusers = users_idusers
        inner join field on idcompany = company_idcompany
        where email = '$email'"));
    }

    # get all companys detail
    function AllCompany()
    {
        return mysqli_fetch_all(querySQL("SELECT idusers , email , company_name , field_name from users
        inner join company
        on idusers = users_idusers
        inner join field
        on idcompany = field.company_idcompany
        where flag = 1"));
    }

    # return one company page
    function OneCompany($idcompany)
    {
        return mysqli_fetch_all(querySQL("SELECT idusers , email , company_name , field_name from users
        inner join company
        on idusers = users_idusers
        inner join field
        on idcompany = field.company_idcompany
        where idusers = '$idcompany'"));
    }

    # return id company
    function IdCompany($companyname = null,$iduser = null)
    {
        if ($companyname !== null){
            $company = mysqli_fetch_assoc(querySQL("SELECT idcompany from company where company_name = '$companyname'"));
            return $company['idcompany'];
        }
        elseif ($iduser !== null){
            $value = mysqli_fetch_assoc(querySQL("select idcompany from company where company.users_idusers = '$iduser'"));
            return $value["idcompany"];
        }
    }

    # return Company Name
    function CompanyName($companyname)
    {
        $id_company = IdCompany($companyname);
        $value = mysqli_fetch_assoc(querySQL("SELECT company_name from company where idcompany = '$id_company'"));
        return $value['company_name'];
    }

    # insert in company
    function InsertCompany($companyname,$email,$field)
    {
        $id_user = IdUser($email);
        querySQL("INSERT INTO `company`(`idcompany`, `company_name`, `users_idusers`) VALUES ('','$companyname','$id_user')");
        $id_company = IdCompany($companyname);
        querySQL("INSERT INTO `field`(`idfield`, `field_name`, `company_idcompany`, `company_users_idusers`)
        VALUES ('','$field','$id_company','$id_user')");
    }

    # update company
    function UpdateCompany($email,$companyname)
    {
        $id_user = IdUser($email);
        querySQL("UPDATE `company` SET `company_name`='$companyname' WHERE users_idusers = '$id_user'");
    }

    # update field
    function UpdateField($companyname,$field)
    {
        $id_company = IdCompany($companyname);
        querySQL("UPDATE `field` SET `field_name`='$field' WHERE company_idcompany = $id_company");
    }
?>