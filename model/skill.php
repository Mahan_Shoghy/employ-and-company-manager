<?php
    # return id skill
    function IdSkill($skill)
    {
        $value = mysqli_fetch_assoc(querySQL("SELECT idskill from skill where skill_name = '$skill'"));
        return $value['idskill'];
    }

    # return all skills of imployer
    function AllSkill($email)
    {
        return mysqli_fetch_all(querySQL("SELECT skill_name from skill inner join imploye_has_skill
        on skill_idskill = idskill
        inner join imploye
        on imploye.imploye_idusers = imploye_has_skill.imploye_idusers
        where imploye.users_idusers = (select idusers from users where email = '$email')"));
    }

    # insert in skill
    function InsertSkill($skill,$email)
    {
        $id_imploye = IdImploye($email);
        foreach ($skill as $value) {
            querySQL("INSERT INTO `skill`(`idskill`, `skill_name`) VALUES ('','$value')");
            $id_skill = IdSkill($value);
            querySQL("INSERT INTO `imploye_has_skill`(`imploye_idusers`, `skill_idskill`) VALUES ('$id_imploye','$id_skill')");
          }
    }

    # return skills of one imploye
    function ImployerSkill($idimployer)
    {
        return mysqli_fetch_all(querySQL("SELECT skill_name from skill inner join imploye_has_skill
            on skill_idskill = idskill
            inner join imploye
            on imploye.imploye_idusers = imploye_has_skill.imploye_idusers
            where imploye.users_idusers = (select idusers from users where idusers = '$idimployer')"));
    }

    # delete from skill
    function DeleteSkill($email)
    {
        $id_imploye = IdImploye($email);
        $ids_skill = mysqli_fetch_all(querySQL("select skill_idskill from imploye_has_skill WHERE imploye_idusers = '$id_imploye'"));
        querySQL("DELETE FROM `imploye_has_skill` WHERE imploye_idusers = '$id_imploye'");
        foreach ($ids_skill as $value) {
            querySQL("DELETE FROM `skill` WHERE idskill = '$value[0]'");
        }
    }
?>