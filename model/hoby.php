<?php
     # insert in hoby
     function InsertHoby($hoby,$email)
     {
         $id_imploye = IdImploye($email);
         foreach ($hoby as $value) {
             querySQL("INSERT INTO `hoby`(`idhoby`, `hoby_name`) VALUES ('','$value')");
             $id_hoby = IdHoby($value);
             querySQL("INSERT INTO `imploye_has_hoby`(`imploye_idusers`, `hoby_idhoby`) VALUES ('$id_imploye','$id_hoby')");
           }
     }

     # return hobys of one imploye with id imploye
    function ImployerHoby($idimployer)
    {
        return mysqli_fetch_all(querySQL("SELECT hoby_name from hoby inner join imploye_has_hoby
            on hoby_idhoby = idhoby
            inner join imploye
            on imploye.imploye_idusers = imploye_has_hoby.imploye_idusers
            where imploye.users_idusers = (select idusers from users where idusers = '$idimployer')"));
    }

    # return all hobys of imployer with emial
    function AllHoby($email)
    {
        return mysqli_fetch_all(querySQL("SELECT hoby_name from hoby inner join imploye_has_hoby
            on hoby_idhoby = idhoby
            inner join imploye
            on imploye.imploye_idusers = imploye_has_hoby.imploye_idusers
            where imploye.users_idusers = (select idusers from users where email = '$email')"));
    }

    # return id hoby
    function IdHoby($hoby)
    {
        $value = mysqli_fetch_assoc(querySQL("SELECT idhoby from hoby where hoby_name = '$hoby'"));
        return $value['idhoby'];
    }

    # delet from hoby
    function DeleteHoby($email)
    {
        $id_imploye = IdImploye($email);
        $ids_hoby = mysqli_fetch_all(querySQL("select hoby_idhoby from imploye_has_hoby WHERE imploye_idusers = '$id_imploye'"));
        querySQL("DELETE FROM `imploye_has_hoby` WHERE imploye_idusers = '$id_imploye'");
        foreach ($ids_hoby as $value) {
            querySQL("DELETE FROM `hoby` WHERE idhoby = '$value[0]'");
        }
    }
?>