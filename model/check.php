<?php
    # return num rows of company name
    function NameCheckCompany($companyname)
    {
        $row = mysqli_num_rows(querySQL("SELECT company_name from company where '$companyname' = company_name"));
        return $row;
    }

    # return num rows of email
    function EmailCheck($email)
    {   
        $row = mysqli_num_rows(querySQL("SELECT email from users where '$email' = email"));
        return $row;
    }

    # return num rows of password
    function PassCheck($password)
    {
        $row = mysqli_num_rows(querySQL("SELECT password from users where password = '$password'"));
        return $row;
    }

    # check company name for edit company
    function NameCheckCompanyEdit($companyname,$email)
    {
        $last_set_name = CompanyName($email);
        $name = mysqli_fetch_assoc(querySQL("SELECT company_name from company where '$companyname' = company_name"));
        if ($name["company_name"] !== $last_set_name && NameCheckCompany($companyname) == 1){
            return 1;
        }
        else{
            return 0;
        }
    }