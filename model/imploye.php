<?php
    # return all detail of one imployer
    function AllFromImploye($email)
    {
        return mysqli_fetch_all(querySQL("SELECT idusers , imploye_idusers , imploye_fullname , imploye_gen , imploye_bdate , imploye_desc , country_name , name_city , degry_name , degry_avg , pnumber from users
        inner join imploye
        on idusers = users_idusers
        inner join country
        on country_idcountry = idcountry
        inner join city on city_id = city.id inner join degry
        on degry_iddegry = iddegry
        where email = '$email'"));
    }

    # return all imployers
    function AllImployers()
    {
        return mysqli_fetch_all(querySQL("SELECT idusers , imploye_fullname , country_name , name_city from users
        inner join imploye
        on idusers = imploye.users_idusers
        inner join country
        on imploye.country_idcountry = country.idcountry
        inner join city
        on imploye.city_id = city.id"));
    }

    # return detail of one imploye
    function OneImploye($idimployer)
    {
        return mysqli_fetch_all(querySQL("SELECT idusers , imploye_idusers , imploye_fullname , imploye_gen , imploye_bdate , imploye_desc , country_name , name_city , degry_name , degry_avg , pnumber from users
            inner join imploye
            on idusers = users_idusers
            inner join country
            on country_idcountry = idcountry
            inner join city
            on city_id = city.id
            inner join degry
            on degry_iddegry = iddegry
            where idusers = '$idimployer'"));
    }

    # return id imploye
    function IdImploye($email)
    {
        $iduser = IdUser($email);
        $value = mysqli_fetch_assoc(querySQL("SELECT imploye_idusers from imploye where users_idusers = '$iduser'"));
        return $value['imploye_idusers'];
    }

    # return imployers who sent resume
    function AllReqforCompany($email)
    {
        $id_company_req = AllRequestCompany($email);
        return mysqli_fetch_all(querySQL("SELECT users_idusers , imploye_fullname from imploye
                inner join requset
                on imploye.imploye_idusers = requset.imploye_idusers
                and
                '$id_company_req' = requset.company_idcompany
                and result = 0"));
    }

    # return birth city of one imploye
    function BirthCity($email)
    {
        return mysqli_fetch_row(querySQL("SELECT name_city from city
        inner join imploye
        on city.id = imploye.bcity_id
        where imploye.users_idusers = (select idusers from users where email = '$email')"));
    }

    # insert in imploye
    function InsertImploye($id_users,$name,$gen,$birthdate,$description,$id_country,$id_degry,$avgdegry,$id_city,$id_bcity)
    {
        querySQL("INSERT INTO `imploye`(`imploye_idusers`, `users_idusers`, `imploye_fullname`, `imploye_gen`, `imploye_bdate`, `imploye_desc`, `country_idcountry`, `degry_iddegry`, `degry_avg` , `city_id`, `bcity_id`)
        VALUES
        ('','$id_users','$name','$gen','$birthdate','$description','$id_country','$id_degry','$avgdegry','$id_city','$id_bcity')");
    }

    # update imploye
    function UpdateImploye($name,$gen,$birthdate,$description,$avgdegry,$country,$degry,$city,$bcity,$email)
    {
        $id_country = IdCountry($country);
        $id_degry = IdDegry($degry);
        $id_city = IdCity($city);
        $id_bcity = IdCity($bcity);
        $id_imploye_users = IdUser($email);
        
        querySQL("UPDATE `imploye` SET `imploye_fullname`='$name',
        `imploye_gen`='$gen',`imploye_bdate`='$birthdate',
        `imploye_desc`='$description',`country_idcountry`='$id_country',
        `degry_iddegry`='$id_degry',`degry_avg`='$avgdegry',`city_id`='$id_city',
        `bcity_id`='$id_bcity' WHERE users_idusers = '$id_imploye_users'");   
    }
?>