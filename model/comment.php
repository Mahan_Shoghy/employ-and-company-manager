<?php
    # return id request
    function IdRequest($email)
    {
        $value = AllFromImploye($email);
        return $value[0][1]; 
    }

    # return public comment imployer
    function PublicCommentImployer($email)
    {
        $id_imploye_req = IdRequest($email);
        return mysqli_fetch_all(querySQL("SELECT `idcomment`, `comment_text`, company_name , `company_idcompany`, `imploye_idusers` FROM `comment`
        inner join company
        on company_idcompany = idcompany
        WHERE comment_view = 1 and imploye_idusers = '$id_imploye_req'"));
    }

    # return All id request
    function AllRequestCompany($email)
    {
        $value = AllFromCompany($email);
        return $value[0][3]; 
    }

    # return id imploye
    function OneIdImploye($iduser)
    {
        $value = mysqli_fetch_assoc(querySQL("SELECT imploye_idusers from imploye where users_idusers = '$iduser'"));
        return $value['imploye_idusers'];
    }

    # return priavet comments
    function PrivetComments($email,$iduser)
    {
        $id_company_req = AllRequestCompany($email);
        $id = OneIdImploye($iduser);
        return mysqli_fetch_all(querySQL("SELECT `idcomment`, `comment_text`, company_name , `company_idcompany`, `imploye_idusers` FROM `comment`
            inner join company
            on company_idcompany = idcompany
            WHERE comment_view = 0 and imploye_idusers = '$id' and idcompany = '$id_company_req'"));
    }

    # return public comments
    function PublicComments($email,$iduser)
    {
        $id_company_req = AllRequestCompany($email);
        $id = OneIdImploye($iduser);
        return mysqli_fetch_all(querySQL("SELECT `idcomment`, `comment_text`, company_name , `company_idcompany`, `imploye_idusers` FROM `comment`
            inner join company
            on company_idcompany = idcompany
            WHERE comment_view = 1 and imploye_idusers = '$id' and idcompany = '$id_company_req'"));
    }

    # insert in comment
    function InsertComment($text,$id_req,$id_imploye,$privet)
    {
        if ($privet == "on"){
            querySQL("INSERT INTO `comment`(`idcomment`, `comment_text`, `comment_view`, `company_idcompany`, `imploye_idusers`)
            VALUES ('','$text',0,'$id_req','$id_imploye')");
        }
        else {
            querySQL("INSERT INTO `comment`(`idcomment`, `comment_text`, `comment_view`, `company_idcompany`, `imploye_idusers`)
            VALUES ('','$text',1,'$id_req','$id_imploye')");
        }
    }
?>