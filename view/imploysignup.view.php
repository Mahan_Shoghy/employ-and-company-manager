<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sign Up</title>
    <script src='https://www.google.com/recaptcha/api.js' async defer ></script>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style media="screen">
      hr {
        border: 0.5px solid;

      }
    </style>
  </head>
  <body>
    <datalist id='city'>
      <?php foreach (Citys() as $value): ?>
        <option><?= $value ?></option>
      <?php endforeach; ?>
    </datalist>
    <datalist id="country">
      <?php foreach (Country() as $value): ?>
        <option><?= $value ?></option>
      <?php endforeach; ?>
    </datalist>
    <datalist id="degry">
      <?php foreach (Degrys() as $value): ?>
        <option><?= $value ?></option>
      <?php endforeach; ?>
    </datalist>
    <div class="wrapper">
      <div class="box">
        <p class="alert alert-danger" role="alert">
          <label>This site just suported Iran</label>
        </p>
        <p class="alert alert-warning" role="alert">
          <label>Are you a Company? Signup <a href="companysignup.controller.php">Here</a></label>
        </p>
        <div class="content_box">
          <form action="" method="post" enctype="application/x-www-form-urlencoded">
            <div class="form-group">
              <div class="form-row">
                <div class="col">
                  <label for="exampleInputname">Name</label>
                  <input type="text" name="name" class="form-control" id="exampleInputname" placeholder="Enter Name" required>
                </div>
                <div class="col">
                  <label for="exampleInputlname">Last Name</label>
                  <input type="text" name="lastname" class="form-control" id="exampleInputlname" placeholder="Enter Last Name" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="gen">Gen: </label>
              <div id="gen" class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-light">
                  <input type="radio" name="gen" id="option1" value="0" autocomplete="off" required> Male
                </label>
                <label class="btn btn-light">
                  <input type="radio" name="gen" id="option2" value="1" autocomplete="off" required> Female
                </label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col">
                  <label for="exampleInputcountry">Country</label>
                  <input type="text" name="country" class="form-control" id="exampleInputbirthcity" placeholder="Enter Country" list="country" required>
                </div>
                <div class="col">
                  <label for="exampleInputcity">City</label>
                  <input type="text" name="city" class="form-control" id="exampleInputcity" placeholder="Enter City" list="city" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col">
                  <label for="exampleInputbirthdate">Birth Date</label>
                  <input type="date" name="birthdate" class="form-control" id="exampleInputbirthdate" required>
                </div>
                <div class="col">
                  <label for="exampleInputbirthcity">Birth City</label>
                  <input type="text" name="birthcity" class="form-control" id="exampleInputbirthcity" placeholder="Enter Birth City" list="city" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputpnumber">Phone Number</label>
              <input type="tel" name="pnumber" class="form-control" id="exampleInputpnumber" placeholder="Enter Phone Number" required>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col">
                  <label for="exampleInputlastdegry">Last Degry</label>
                  <input type="text" name="lastdegry" class="form-control" id="exampleInputlastdegry" placeholder="Enter Last Degry" list="degry" required>
                </div>
                <div class="col">
                  <label for="exampleInputavgdegry">Avrege</label>
                  <input type="number" name="avgdegry" class="form-control" id="exampleInputavgdegry" placeholder="Enter AVG Degry" max="20" min="0" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email Address</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email" required>
            </div>
            <label for="jobxp">Job Experience</label>
            <div id="jobxp" class="form-group" style="border:2px solid; border-radius: 10px; padding: 20px;">
              <div class="form-group">
                <div class="form-row">
                  <div class="col">
                    <label for="exampleInputcompany">Company</label>
                    <input type="text" name="company[]" class="form-control" id="exampleInputcompany" placeholder="Enter Company">
                  </div>
                  <div class="col">
                    <label for="exampleInputstartdate">Start Date</label>
                    <input type="date" name="startdate[]" class="form-control" id="exampleInputstartdate">
                  </div>
                  <div class="col">
                    <label for="exampleInputenddate">End Date</label>
                    <input type="date" name="enddate[]" class="form-control" id="exampleInputenddate">
                  </div>
                  <div>
                    <label for="exampleInputreason">Reason for separation</label>
                    <textarea name="reason[]" class="form-control" id="exampleInputreason" placeholder="Enter Reason" cols="60" rows="3"></textarea>
                  </div>
                </div>
              </div>
              <hr>
              <div class="form-group">
                <div class="form-row">
                  <div class="col">
                    <label for="exampleInputcompany">Company</label>
                    <input type="text" name="company[]" class="form-control" id="exampleInputcompany" placeholder="Enter Company">
                  </div>
                  <div class="col">
                    <label for="exampleInputstartdate">Start Date</label>
                    <input type="date" name="startdate[]" class="form-control" id="exampleInputstartdate">
                  </div>
                  <div class="col">
                    <label for="exampleInputenddate">End Date</label>
                    <input type="date" name="enddate[]" class="form-control" id="exampleInputenddate">
                  </div>
                  <div>
                    <label for="exampleInputreason">Reason for separation</label>
                    <textarea name="reason[]" class="form-control" id="exampleInputreason" placeholder="Enter Reason" cols="60" rows="3"></textarea>
                  </div>
                </div>
              </div>
              <hr>
              <div class="form-group">
                <div class="form-row">
                  <div class="col">
                    <label for="exampleInputcompany">Company</label>
                    <input type="text" name="company[]" class="form-control" id="exampleInputcompany" placeholder="Enter Company">
                  </div>
                  <div class="col">
                    <label for="exampleInputstartdate">Start Date</label>
                    <input type="date" name="startdate[]" class="form-control" id="exampleInputstartdate">
                  </div>
                  <div class="col">
                    <label for="exampleInputenddate">End Date</label>
                    <input type="date" name="enddate[]" class="form-control" id="exampleInputenddate">
                  </div>
                  <div>
                    <label for="exampleInputreason">Reason for separation</label>
                    <textarea name="reason[]" class="form-control" id="exampleInputreason" placeholder="Enter Reason" cols="60" rows="3"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputdescription">Description</label>
              <textarea name="description" class="form-control" id="exampleInputdescription" placeholder="Enter Description" rows="4" required></textarea>
            </div>
            <div class="form-group">
              <p class="alert alert-warning" role="alert">
                <label>Use ( , ) between values</label>
              </p>
              <div class="form-row">
                <div class="col">
                  <label for="exampleInputskill">Skill</label>
                  <input type="text" name="skill" class="form-control" id="exampleInputskill" placeholder="Enter Skill" required>
                </div>
                <div class="col">
                  <label for="exampleInputhoby">Hoby</label>
                  <input type="text" name="hoby" class="form-control" id="exampleInputhoby" placeholder="Enter Hoby" required>
                </div>
              </div>
            </div>
            <?php echo genCaptcha(); ?>
            <input type="reset" class="btn btn-primary" value="Reset">
            <input type="submit" class="btn btn-primary" name="signup" value="Sign up">
            <p>
              <?php
                if (isset($message_error)) {
                  foreach ($message_error[0] as $value) {
                    if ($value != null) {
                      echo "<span class='alert alert-danger' role='alert'>$value</span><br>";
                    }
                  }
                }
              ?>
            </p>
          </form>
        </div>
        <p class="alert alert-dark" role="alert" style="margin-top:20px;">
          <label>Already have an account? <a href="login.controller.php">Login</a></label>
        </p>
      </div>
    </div>
  </body>
</html>
