<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <script src='https://www.google.com/recaptcha/api.js' async defer ></script>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <div class="wrapper">
      <div class="box">
        <?php if (isset($_SESSION["password"])): ?>
          <div class="alert alert-info" role="alert">
            Your Password is: <?php echo $_SESSION["password"] ?> Please save it!
          </div>
        <?php endif; ?>
        <div class="content_box">
          <form action="" method="post" enctype="application/x-www-form-urlencoded">
            <div class="form-group">
              <label for="exampleInputUsername">Email Address</label>
              <input type="text" name="email" class="form-control" id="exampleInputUsername" aria-describedby="emailHelp" placeholder="Enter email" <?php if (isset($_SESSION["email"])): ?>
                <?php echo $_SESSION["email"] ?>
              <?php endif; ?> required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php if (isset($_SESSION["password"])): ?>
                <?php echo $_SESSION["password"] ?>
              <?php endif; ?>" required>
            </div>
            <input type="checkbox" name="rememberme"> Remember Me
            <?php echo genCaptcha(); ?>
            <br><br>
            <input type="submit" class="btn btn-primary" name="login" value="Login">
            <p>
              <?php
                if (isset($message_error)) {
                  echo "<span class='alert alert-danger' role='alert'>$message_error</span><br>";
                }
              ?>
            </p>
          </form>
        </div>
        <p class="alert alert-dark" role="alert" style="margin-top:20px;">
          <label>Dont't have an account? <a href="imploysignup.controller.php">Sign Up</a></label>
        </p>
      </div>
    </div>
  </body>
</html>
