<?php
  include"../model/model.php";
  $email = $_SESSION["login-company"];
  $company_detail = AllFromCompany($email);
  $phonecompany = PhoneCompany($email);
  $allimployers = AllImployers();
  $reqforcompany = AllReqforCompany($email);
  if (isset($_GET["idimployer"])){
    $oneimployer = OneImploye($_GET["idimployer"]);
    $oneimployer_skill = ImployerSkill($_GET["idimployer"]);
    $oneimployer_hoby = ImployerHoby($_GET["idimployer"]);
    $oneimployer_jobxp = ImployerJobxp($_GET["idimployer"]);
    $pracomment_company = PrivetComments($email,$_GET["idimployer"]);
    $pabcomment_company = PublicComments($email,$_GET["idimployer"]);
    $idrequest = IdRequestResult($email,$_GET["idimployer"]);
  }
?>
<div class="jumbotron mt-3">
  <h1><?= $company_detail[0][1] ?></h1>
  <p class="lead"><?= $company_detail[0][2] ?></p>
  <a class="btn btn-lg btn-primary" href="edit.controller.php" role="button">Edit Profile</a>
  <a class="btn btn-lg btn-dark" style="float:right;" href="logout.controller.php" role="button">Logout</a>
</div>
