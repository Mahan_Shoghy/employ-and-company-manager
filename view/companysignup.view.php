<!DOCTYPE html>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sign Up</title>
    <script src='https://www.google.com/recaptcha/api.js' async defer ></script>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style media="screen">
      hr {
        border: 0.5px solid;

      }
    </style>
  </head>
  <body>
    <div class="wrapper">
      <div class="box">
        <p class="alert alert-warning" role="alert">
          <label>Are you a Imployer? Signup <a href="imploysignup.controller.php">Here</a></label>
        </p>
        <div class="content_box">
          <form action="" method="post" enctype="application/x-www-form-urlencoded">
            <div class="form-group">
              <label for="exampleInputcompanyname">Company Name</label>
              <input type="text" name="companyname" class="form-control" id="exampleInputcompanyname" placeholder="Enter Company Name" >
            </div>
            <div class="form-group">
              <label for="exampleInputpnumber">Phone Number</label>
              <input type="tel" name="pnumber" class="form-control" id="exampleInputpnumber" placeholder="Enter Phone Number" >
            </div>
            <div class="form-group">
              <label for="exampleInputfield">Field</label>
              <input type="text" name="field" class="form-control" id="exampleInputfield" placeholder="Enter Field" >
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email Address</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email" >
            </div>
            <div class="form-group">
              <label for="exampleInputpassword">Password</label>
              <input type="password" name="password" class="form-control" id="exampleInputpassword" placeholder="Enter Password" >
            </div>
            <?php echo genCaptcha(); ?>
            <input type="reset" class="btn btn-primary" value="Reset">
            <input type="submit" class="btn btn-primary" name="signup" value="Sign up">
            <p>
              <?php
                if (isset($message_error)) {
                  foreach ($message_error as $value) {
                    if ($value != null) {
                      echo "<span class='alert alert-danger' role='alert'>$value</span><br>";
                    }
                  }
                }
              ?>
            </p>
          </form>
        </div>
        <p class="alert alert-dark" role="alert" style="margin-top:20px;">
          <label>Already have an account? <a href="login.controller.php">Login</a></label>
        </p>
      </div>
    </div>
  </body>
</html>
