<nav class="navbar fixed-bottom navbar-expand-sm navbar-dark bg-dark">
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="dashboard.controller.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="list.company.controller.php">Companys</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="sentresume.controller.php">Sent Resume</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="search.imploye.controller.php" method="get">
      <div class="btn-group dropup">
        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Filter
        </button>
        <div class="dropdown-menu">
          <div class="p btn-group-toggle btn-group-vertical" data-toggle="buttons">
          <label class="btn btn-info">
            <input type="radio" name="typefilter" id="option1" value="0" autocomplete="off"> Field
          </label>
          <label class="btn btn-info">
            <input type="radio" name="typefilter" id="option2" value="1" autocomplete="off"> Company
          </label>
        </div>
        </div>
      </div>
      <input class="form-control mr-sm-2" type="search" name="textsearch" placeholder="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
    </form>
  </div>
</nav>
