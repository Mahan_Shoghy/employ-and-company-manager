<div class="jumbotron mt-3">
<?php
  include"../model/model.php";
  $email = $_SESSION["login-imploye"];
  $imploye_detail = AllFromImploye($email);
  $imploye_skill = AllSkill($email);
  $imploye_hoby = AllHoby($email);
?>
  <h1><?= $imploye_detail[0][2] ?></h1>
  <p class="lead"><?php foreach ($imploye_skill as $value) {
    echo $value[0].', ';
  } ?></p>
  <a class="btn btn-lg btn-primary" href="edit.controller.php" role="button">Edit Profile</a>
  <a class="btn btn-lg btn-dark" style="float:right;" href="logout.controller.php" role="button">Logout</a>
</div>
