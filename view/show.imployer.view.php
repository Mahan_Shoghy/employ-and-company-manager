<?php
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Dashboard</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <style type="text/css">.mx-tip-panel:focus{outline:0;}
      .mx_keyword{line-height:20px; color:#333333; font-size:14px; letter-spacing: 0.2px;font-family:PingFangSC-Semibold;}
      .mx-scrollbar::-webkit-scrollbar{width:8px;height:1px;}
      .mx-scrollbar{overflow-y:auto; max-height:380px; }
      .mx-scrollbar::-webkit-scrollbar-thumb{border-radius:4px; background: #C0C0C0;}
      .mx-scrollbar::-webkit-scrollbar-track{border-radius:4px; background: #eeeeee;}
      .maxthon_tip{background-color:white; border:none;} .maxthon_tip:active{background-color: #E6F4FF;border:1px solid #449EFB; } .maxthon_tip:hover{background-color:#E6F4FF; border:1px solid #449EFB;}
    </style></head>
<body>
  <div class="container" style="margin-bottom:100px;">
    <?php include"navbar.company.view.php"; ?>
    <div class="inbox_box">
      <br>
      <p>Full Name: <?php echo $oneimployer[0][2] ?></p>
      <p>Country: <?php echo $oneimployer[0][6] ?></p>
      <p>City: <?php echo $oneimployer[0][7] ?></p>
      <p>Phone Number: <?php echo $oneimployer[0][10] ?></p>
      <p>Last Degry: <?php echo $oneimployer[0][8] ?></p>
      <p>Avrage: <?php echo $oneimployer[0][9] ?></p>
      <p>Birth Date: <?php echo $oneimployer[0][4] ?></p>
      <p>Skill: <?php foreach ($oneimployer_skill as $value) {
        echo $value[0].', ';
      } ?></p>
      <p>Hoby: <?php foreach ($oneimployer_hoby as $value) {
        echo $value[0].', ';
      } ?></p>
      <label>Job Experienc:</label>
      <p> <?php foreach ($oneimployer_jobxp as $key => $value): ?>
        <span>Company Name: <?php echo $oneimployer_jobxp[$key][0] ?></span> ,
        <span>Start Date: <?php echo $oneimployer_jobxp[$key][1] ?></span> ,
        <span>End Date: <?php echo $oneimployer_jobxp[$key][2] ?></span> ,
        <span>Reason: <?php echo $oneimployer_jobxp[$key][3] ?></span>
        <br>
      <?php endforeach; ?></p>
    </div>
    <div class="inbox_box">
      <label>Comments:</label><br>
      <?php if (!empty($pracomment_company)): ?>
        <?php foreach ($pracomment_company as $value): ?>
          <div style="padding:10px; border:2px solid; border-radius:10px;">
            <p>Company Name: <?php echo $value[2] ?></p>
            <p>Message: <?php echo $value[1] ?></p>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
      <?php if (!empty($pabcomment_company)): ?>
        <?php foreach ($pabcomment_company as $value): ?>
          <div style="padding:10px; border:2px solid; border-radius:10px;">
            <p>Company Name: <?php echo $value[2] ?></p>
            <p>Message: <?php echo $value[1] ?></p>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
    <div class="inbox_box">
      <br>
      <label>Add Comment:</label>
      <form class="" action="addcomment.controller.php?idimployer=<?php echo $oneimployer[0][1] ?>" method="post">
        <textarea name="text" class="form-control" rows="8" cols="80" placeholder="Enter Comment"></textarea><br>
        <input type="checkbox" name="priavet"> Priavet <br><br>
        <input type="submit" class="btn btn-primary" name="send" value="Send">
      </form>
    </div>
  </div>
  <?php include"footer.company.view.php"; ?>
</body>
</html>
