<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Dashboard</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <style type="text/css">.mx-tip-panel:focus{outline:0;}
      .mx_keyword{line-height:20px; color:#333333; font-size:14px; letter-spacing: 0.2px;font-family:PingFangSC-Semibold;}
      .mx-scrollbar::-webkit-scrollbar{width:8px;height:1px;}
      .mx-scrollbar{overflow-y:auto; max-height:380px; }
      .mx-scrollbar::-webkit-scrollbar-thumb{border-radius:4px; background: #C0C0C0;}
      .mx-scrollbar::-webkit-scrollbar-track{border-radius:4px; background: #eeeeee;}
      .maxthon_tip{background-color:white; border:none;} .maxthon_tip:active{background-color: #E6F4FF;border:1px solid #449EFB; } .maxthon_tip:hover{background-color:#E6F4FF; border:1px solid #449EFB;}
    </style></head>
<body>
  <div class="container" style="margin-bottom:100px;">
    <?php include"navbar.imploye.view.php"; ?>
    <div class="inbox_box">
      <?php
        $email = $_SESSION["login-imploye"];
        $imploye_detail = AllFromImploye($email);
        $imploye_skill = AllSkill($email);
        $imploye_hoby = AllHoby($email);
        $imploye_bcity = BirthCity($email);
      ?>
      <datalist id='city'>
        <?php foreach (Citys() as $value): ?>
          <option><?= $value ?></option>
        <?php endforeach; ?>
      </datalist>
      <datalist id="country">
        <?php foreach (Country() as $value): ?>
          <option><?= $value ?></option>
        <?php endforeach; ?>
      </datalist>
      <datalist id="degry">
        <?php foreach (Degrys() as $value): ?>
          <option><?= $value ?></option>
        <?php endforeach; ?>
      </datalist>
      <div class="wrapper">
        <div class="box">
          <div class="content_box">
            <form action="" method="post" enctype="application/x-www-form-urlencoded">
              <div class="form-group">
                <div class="form-row">
                  <label for="exampleInputfullname">Full Name</label>
                  <input type="text" name="fullname" class="form-control" id="exampleInputfullname" placeholder="Enter Full Name" value="<?php echo $imploye_detail[0][2] ?>" required>
                </div>
              </div>
              <div class="form-group">
                <label for="gen">Gen: </label>
                <div id="gen" class="btn-group btn-group-toggle" data-toggle="buttons">
                  <label class="btn btn-light">
                    <input type="radio" name="gen" id="option1" value="0" autocomplete="off" required> Male
                  </label>
                  <label class="btn btn-light">
                    <input type="radio" name="gen" id="option2" value="1" autocomplete="off" required> Female
                  </label>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col">
                    <label for="exampleInputcountry">Country</label>
                    <input type="text" name="country" class="form-control" id="exampleInputbirthcity" placeholder="Enter Country" list="country" value="<?php echo $imploye_detail[0][6] ?>" required>
                  </div>
                  <div class="col">
                    <label for="exampleInputcity">City</label>
                    <input type="text" name="city" class="form-control" id="exampleInputcity" placeholder="Enter City" list="city" value="<?php echo $imploye_detail[0][7] ?>" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col">
                    <label for="exampleInputbirthdate">Birth Date</label>
                    <input type="date" name="birthdate" class="form-control" id="exampleInputbirthdate" value="<?php echo $imploye_detail[0][4] ?>" required>
                  </div>
                  <div class="col">
                    <label for="exampleInputbirthcity">Birth City</label>
                    <input type="text" name="birthcity" class="form-control" id="exampleInputbirthcity" placeholder="Enter Birth City" list="city" value="<?php echo $imploye_bcity[0] ?>" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputpnumber">Phone Number</label>
                <input type="tel" name="pnumber" class="form-control" id="exampleInputpnumber" placeholder="Enter Phone Number" value="<?php echo $imploye_detail[0][10] ?>" required>
              </div>
              <div class="form-group">
                <div class="form-row">
                  <div class="col">
                    <label for="exampleInputlastdegry">Last Degry</label>
                    <input type="text" name="lastdegry" class="form-control" id="exampleInputlastdegry" placeholder="Enter Last Degry" list="degry" value="<?php echo $imploye_detail[0][8] ?>" required>
                  </div>
                  <div class="col">
                    <label for="exampleInputavgdegry">Avrege</label>
                    <input type="number" name="avgdegry" class="form-control" id="exampleInputavgdegry" placeholder="Enter AVG Degry" max="20" min="0" value="<?php echo $imploye_detail[0][9] ?>" required>
                  </div>
                </div>
              </div>
              <label for="jobxp">Add Job Experience</label>
              <div id="jobxp" class="form-group" style="border:2px solid; border-radius: 10px; padding: 20px;">
                <div class="form-group">
                  <div class="form-row">
                    <div class="col">
                      <label for="exampleInputcompany">Company</label>
                      <input type="text" name="company[]" class="form-control" id="exampleInputcompany" placeholder="Enter Company">
                    </div>
                    <div class="col">
                      <label for="exampleInputstartdate">Start Date</label>
                      <input type="date" name="startdate[]" class="form-control" id="exampleInputstartdate">
                    </div>
                    <div class="col">
                      <label for="exampleInputenddate">End Date</label>
                      <input type="date" name="enddate[]" class="form-control" id="exampleInputenddate">
                    </div>
                    <div>
                      <label for="exampleInputreason">Reason for separation</label>
                      <textarea name="reason[]" class="form-control" id="exampleInputreason" placeholder="Enter Reason" cols="60" rows="3"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputdescription">Description</label>
                <textarea name="description" class="form-control" id="exampleInputdescription" placeholder="Enter Description" rows="4" required><?php echo $imploye_detail[0][5] ?></textarea>
              </div>
              <div class="form-group">
                <p class="alert alert-warning" role="alert">
                  <label>Use ( , ) between values</label>
                </p>
                <div class="form-row">
                  <div class="col">
                    <label for="exampleInputskill">Skill</label>
                    <input type="text" name="skill" class="form-control" id="exampleInputskill" placeholder="Enter Skill" value="<?php foreach ($imploye_skill as $value) {
                      echo $value[0].', ';
                    } ?>" required>
                  </div>
                  <div class="col">
                    <label for="exampleInputhoby">Hoby</label>
                    <input type="text" name="hoby" class="form-control" id="exampleInputhoby" placeholder="Enter Hoby" value="<?php foreach ($imploye_hoby as $value) {
                      echo $value[0].', ';
                    } ?>" required>
                  </div>
                </div>
              </div>
              <input type="reset" class="btn btn-primary" value="Reset">
              <input type="submit" class="btn btn-primary" name="edit" value="Edit">
              <p>
                <?php
                  if (isset($message_error[0])) {
                    foreach ($message_error[0] as $key => $value) {
                      echo "<span class='alert alert-danger' role='alert' style='margin-left:15px;'>$value</span><br><br>";
                    }
                  }
                  if (isset($message_error[1])) {
                    echo "<span div class='alert alert-success' role='alert' style='margin-left:15px;'>$message_error[1]</span><br>";
                  }
                ?>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include"footer.imploye.view.php"; ?>
</body>
</html>
