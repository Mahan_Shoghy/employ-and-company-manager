<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Dashboard</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <style type="text/css">.mx-tip-panel:focus{outline:0;}
      .mx_keyword{line-height:20px; color:#333333; font-size:14px; letter-spacing: 0.2px;font-family:PingFangSC-Semibold;}
      .mx-scrollbar::-webkit-scrollbar{width:8px;height:1px;}
      .mx-scrollbar{overflow-y:auto; max-height:380px; }
      .mx-scrollbar::-webkit-scrollbar-thumb{border-radius:4px; background: #C0C0C0;}
      .mx-scrollbar::-webkit-scrollbar-track{border-radius:4px; background: #eeeeee;}
      .maxthon_tip{background-color:white; border:none;} .maxthon_tip:active{background-color: #E6F4FF;border:1px solid #449EFB; } .maxthon_tip:hover{background-color:#E6F4FF; border:1px solid #449EFB;}
    </style></head>
<body>
  <div class="container">
    <?php include"navbar.imploye.view.php"; ?>
    <div class="inbox_box">
      <div class="col-lg-12">
        <form class="" action="" method="post">
        </form>
          <table class="table table-striped" id="inbox">
              <thead>
              <th scope="col">#</th>
              <th scope="col">Company Name</th>
              <th scope="col">Fields</th>
              <th scope="col"></th>
              <th scope="col"></th>
              </thead>
          </table>
          <div class="inbox_box">
            <?php
              $email = $_SESSION["login-imploye"];
              $allcompanys = AllCompany();

              foreach ($allcompanys as $key => $value): ?>
              <?php $key += 1 ?>
              <div class="">
                <div style="height:5px; display:block"><a style="text-decoration: none; color: #000;" href="show.company.controller.php?idcompany=<?php echo $value[0] ?>">
                <div style="display:inline-block; float:left; width:14%; margin-left:12px;"><?php echo $key ?></div>
                <div style="display:inline-block; float:left; width:43%;"><?php echo $value[2] ?></div>
                <div style="display:inline-block; float:left; width:31%;"><?php echo $value[3] ?></div>
                <div style="display:inline-block; float:left; width:15%;"></div></a></div>
                <button class="btn btn-sm btn-primary" type="button" name="sendreq"><a style="text-decoration: none; color: #000;" href="sendreq.controller.php?idcompany=<?php echo $value[0] ?>">Send Resume</a></button>
                <br><hr>
              </div>
            <?php endforeach; ?>
          </div>
      </div>
    </div>
  </div>
  <?php include"footer.imploye.view.php"; ?>
</body>
</html>
