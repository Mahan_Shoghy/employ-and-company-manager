-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2019 at 01:29 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cv`
--

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

CREATE TABLE `block` (
  `ipuser` varchar(20) NOT NULL,
  `attemp` int(1) NOT NULL,
  `blocktime` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `block`
--

INSERT INTO `block` (`ipuser`, `attemp`, `blocktime`) VALUES
('::1', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name_city` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name_city`) VALUES
(2, 'اردبيل'),
(3, 'اصلاندوز'),
(4, 'آبي بيگلو'),
(5, 'بيله سوار'),
(6, 'پارس آباد'),
(7, 'تازه كند'),
(8, 'تازه كندانگوت'),
(9, 'جعفرآباد'),
(10, 'خلخال'),
(11, 'رضي'),
(12, 'سرعين'),
(13, 'عنبران'),
(14, 'فخرآباد'),
(15, 'كلور'),
(16, 'كوراييم'),
(17, 'گرمي'),
(18, 'گيوي'),
(19, 'لاهرود'),
(20, 'مرادلو'),
(21, 'مشگين شهر'),
(22, 'نمين'),
(23, 'نير'),
(24, 'هشتجين'),
(25, 'هير'),
(26, 'ابريشم'),
(27, 'ابوزيدآباد'),
(28, 'اردستان'),
(29, 'اژيه'),
(30, 'اصفهان'),
(31, 'افوس'),
(32, 'انارك'),
(33, 'ايمانشهر'),
(34, 'آران وبيدگل'),
(35, 'بادرود'),
(36, 'باغ بهادران'),
(37, 'بافران'),
(38, 'برزك'),
(39, 'برف انبار'),
(40, 'بوئين ومياندشت'),
(41, 'بهاران شهر'),
(42, 'بهارستان'),
(43, 'پيربكران'),
(44, 'تودشك'),
(45, 'تيران'),
(46, 'جندق'),
(47, 'جوزدان'),
(48, 'جوشقان وكامو'),
(49, 'چادگان'),
(50, 'چرمهين'),
(51, 'چمگردان'),
(52, 'حبيب آباد'),
(53, 'حسن آباد'),
(54, 'حنا'),
(55, 'خالدآباد'),
(56, 'خميني شهر'),
(57, 'خوانسار'),
(58, 'خور'),
(59, 'خوراسگان'),
(60, 'خورزوق'),
(61, 'داران'),
(62, 'دامنه'),
(63, 'درچه پياز'),
(64, 'دستگرد'),
(65, 'دولت آباد'),
(66, 'دهاقان'),
(67, 'دهق'),
(68, 'ديزيچه'),
(69, 'رزوه'),
(70, 'رضوانشهر'),
(71, 'زاينده رود'),
(72, 'زرين شهر'),
(73, 'زواره'),
(74, 'زيباشهر'),
(75, 'سده لنجان'),
(76, 'سفيدشهر'),
(77, 'سگزي'),
(78, 'سميرم'),
(79, 'شاپورآباد'),
(80, 'شاهين شهر'),
(81, 'شهرضا'),
(82, 'طالخونچه'),
(83, 'عسگران'),
(84, 'علويچه'),
(85, 'فرخي'),
(86, 'فريدونشهر'),
(87, 'فلاورجان'),
(88, 'فولادشهر'),
(89, 'قمصر'),
(90, 'قهجاورستان'),
(91, 'قهدريجان'),
(92, 'كاشان'),
(93, 'كركوند'),
(94, 'كليشادوسودرجان'),
(95, 'كمشچه'),
(96, 'كمه'),
(97, 'كوشك'),
(98, 'كوهپايه'),
(99, 'كهريزسنگ'),
(100, 'گرگاب'),
(101, 'گزبرخوار'),
(102, 'گلپايگان'),
(103, 'گلدشت'),
(104, 'گلشن'),
(105, 'گلشهر'),
(106, 'گوگد'),
(107, 'لاي بيد'),
(108, 'مباركه'),
(109, 'محمدآباد'),
(110, 'مشكات'),
(111, 'منظريه'),
(112, 'مهاباد'),
(113, 'ميمه'),
(114, 'نائين'),
(115, 'نجف آباد'),
(116, 'نصرآباد'),
(117, 'نطنز'),
(118, 'نوش آباد'),
(119, 'نياسر'),
(120, 'نيك آباد'),
(121, 'ورزنه'),
(122, 'ورنامخواست'),
(123, 'وزوان'),
(124, 'ونك'),
(125, 'هرند'),
(126, 'اشتهارد'),
(127, 'آسارا'),
(128, 'تنكمان'),
(129, 'چهارباغ'),
(130, 'سيف آباد'),
(131, 'شهرجديدهشتگرد'),
(132, 'طالقان'),
(133, 'كرج'),
(134, 'كمال شهر'),
(135, 'كوهسار'),
(136, 'گرمدره'),
(137, 'ماهدشت'),
(138, 'محمدشهر'),
(139, 'مشكين دشت'),
(140, 'نظرآباد'),
(141, 'هشتگرد'),
(142, 'اركواز'),
(143, 'ايلام'),
(144, 'ايوان'),
(145, 'آبدانان'),
(146, 'آسمان آباد'),
(147, 'بدره'),
(148, 'پهله'),
(149, 'توحيد'),
(150, 'چوار'),
(151, 'دره شهر'),
(152, 'دلگشا'),
(153, 'دهلران'),
(154, 'زرنه'),
(155, 'سراب باغ'),
(156, 'سرابله'),
(157, 'صالح آباد'),
(158, 'لومار'),
(159, 'مورموري'),
(160, 'موسيان'),
(161, 'مهران'),
(162, 'ميمه'),
(163, 'اسكو'),
(164, 'اهر'),
(165, 'ايلخچي'),
(166, 'آبش احمد'),
(167, 'آذرشهر'),
(168, 'آقكند'),
(169, 'باسمنج'),
(170, 'بخشايش'),
(171, 'بستان آباد'),
(172, 'بناب'),
(173, 'بناب جديد'),
(174, 'تبريز'),
(175, 'ترك'),
(176, 'تركمانچاي'),
(177, 'تسوج'),
(178, 'تيكمه داش'),
(179, 'جلفا'),
(180, 'خاروانا'),
(181, 'خامنه'),
(182, 'خراجو'),
(183, 'خسروشهر'),
(184, 'خمارلو'),
(185, 'خواجه'),
(186, 'دوزدوزان'),
(187, 'زرنق'),
(188, 'زنوز'),
(189, 'سراب'),
(190, 'سردرود'),
(191, 'سيس'),
(192, 'سيه رود'),
(193, 'شبستر'),
(194, 'شربيان'),
(195, 'شرفخانه'),
(196, 'شندآباد'),
(197, 'شهرجديدسهند'),
(198, 'صوفيان'),
(199, 'عجب شير'),
(200, 'قره آغاج'),
(201, 'كشكسراي'),
(202, 'كلوانق'),
(203, 'كليبر'),
(204, 'كوزه كنان'),
(205, 'گوگان'),
(206, 'ليلان'),
(207, 'مراغه'),
(208, 'مرند'),
(209, 'ملكان'),
(210, 'ممقان'),
(211, 'مهربان'),
(212, 'ميانه'),
(213, 'نظركهريزي'),
(214, 'وايقان'),
(215, 'ورزقان'),
(216, 'هاديشهر'),
(217, 'هريس'),
(218, 'هشترود'),
(219, 'هوراند'),
(220, 'يامچي'),
(221, 'اروميه'),
(222, 'اشنويه'),
(223, 'ايواوغلي'),
(224, 'آواجيق'),
(225, 'باروق'),
(226, 'بازرگان'),
(227, 'بوكان'),
(228, 'پلدشت'),
(229, 'پيرانشهر'),
(230, 'تازه شهر'),
(231, 'تكاب'),
(232, 'چهاربرج'),
(233, 'خليفان'),
(234, 'خوي'),
(235, 'ديزج ديز'),
(236, 'ربط'),
(237, 'سردشت'),
(238, 'سرو'),
(239, 'سلماس'),
(240, 'سيلوانه'),
(241, 'سيمينه'),
(242, 'سيه چشمه'),
(243, 'شاهين دژ'),
(244, 'شوط'),
(245, 'فيرورق'),
(246, 'قره ضياءالدين'),
(247, 'قطور'),
(248, 'قوشچي'),
(249, 'كشاورز'),
(250, 'گردكشانه'),
(251, 'ماكو'),
(252, 'محمديار'),
(253, 'محمودآباد'),
(254, 'مهاباد'),
(255, 'مياندوآب'),
(256, 'ميرآباد'),
(257, 'نالوس'),
(258, 'نقده'),
(259, 'نوشين'),
(260, 'امام حسن'),
(261, 'انارستان'),
(262, 'اهرم'),
(263, 'آبپخش'),
(264, 'آبدان'),
(265, 'برازجان'),
(266, 'بردخون'),
(267, 'بردستان'),
(268, 'بندردير'),
(269, 'بندرديلم'),
(270, 'بندرريگ'),
(271, 'بندركنگان'),
(272, 'بندرگناوه'),
(273, 'بنك'),
(274, 'بوشهر'),
(275, 'تنگ ارم'),
(276, 'جم'),
(277, 'چغادك'),
(278, 'خارك'),
(279, 'خورموج'),
(280, 'دالكي'),
(281, 'دلوار'),
(282, 'ريز'),
(283, 'سعدآباد'),
(284, 'سيراف'),
(285, 'شبانكاره'),
(286, 'شنبه'),
(287, 'عسلويه'),
(288, 'كاكي'),
(289, 'كلمه'),
(290, 'نخل تقي'),
(291, 'وحدتيه'),
(292, 'ارجمند'),
(293, 'اسلامشهر'),
(294, 'انديشه'),
(295, 'آبسرد'),
(296, 'آبعلي'),
(297, 'باغستان'),
(298, 'باقرشهر'),
(299, 'بومهن'),
(300, 'پاكدشت'),
(301, 'پرديس'),
(302, 'پيشوا'),
(303, 'تجريش'),
(304, 'تهران'),
(305, 'جوادآباد'),
(306, 'چهاردانگه'),
(307, 'حسن آباد'),
(308, 'دماوند'),
(309, 'رباط كريم'),
(310, 'رودهن'),
(311, 'ري'),
(312, 'شاهدشهر'),
(313, 'شريف آباد'),
(314, 'شهريار'),
(315, 'صالح آباد'),
(316, 'صباشهر'),
(317, 'صفادشت'),
(318, 'فردوسيه'),
(319, 'فرون آباد'),
(320, 'فشم'),
(321, 'فيروزكوه'),
(322, 'قدس'),
(323, 'قرچك'),
(324, 'كهريزك'),
(325, 'كيلان'),
(326, 'گلستان'),
(327, 'لواسان'),
(328, 'ملارد'),
(329, 'نسيم شهر'),
(330, 'نصيرآباد'),
(331, 'وحيديه'),
(332, 'ورامين'),
(333, 'اردل'),
(334, 'آلوني'),
(335, 'باباحيدر'),
(336, 'بروجن'),
(337, 'بلداجي'),
(338, 'بن'),
(339, 'جونقان'),
(340, 'چلگرد'),
(341, 'سامان'),
(342, 'سفيددشت'),
(343, 'سودجان'),
(344, 'سورشجان'),
(345, 'شلمزار'),
(346, 'شهركرد'),
(347, 'طاقانك'),
(348, 'فارسان'),
(349, 'فرادنبه'),
(350, 'فرخ شهر'),
(351, 'كيان'),
(352, 'گندمان'),
(353, 'گهرو'),
(354, 'لردگان'),
(355, 'مال خليفه'),
(356, 'ناغان'),
(357, 'نافچ'),
(358, 'نقنه'),
(359, 'هفشجان'),
(360, 'ارسك'),
(361, 'اسديه'),
(362, 'اسفدن'),
(363, 'اسلاميه'),
(364, 'آرين شهر'),
(365, 'آيسك'),
(366, 'بشرويه'),
(367, 'بيرجند'),
(368, 'حاجي آباد'),
(369, 'خضري دشت بياض'),
(370, 'خوسف'),
(371, 'زهان'),
(372, 'سرايان'),
(373, 'سربيشه'),
(374, 'سه قلعه'),
(375, 'شوسف'),
(376, 'طبس مسينا'),
(377, 'فردوس'),
(378, 'قائن'),
(379, 'قهستان'),
(380, 'گزيك'),
(381, 'محمدشهر'),
(382, 'مود'),
(383, 'نهبندان'),
(384, 'نيمبلوك'),
(385, 'احمدآبادصولت'),
(386, 'انابد'),
(387, 'باجگيران'),
(388, 'باخرز'),
(389, 'بار'),
(390, 'بايگ'),
(391, 'بجستان'),
(392, 'بردسكن'),
(393, 'بيدخت'),
(394, 'تايباد'),
(395, 'تربت جام'),
(396, 'تربت حيدريه'),
(397, 'جغتاي'),
(398, 'جنگل'),
(399, 'چاپشلو'),
(400, 'چكنه'),
(401, 'چناران'),
(402, 'خرو'),
(403, 'خليل آباد'),
(404, 'خواف'),
(405, 'داورزن'),
(406, 'درگز'),
(407, 'درود'),
(408, 'دولت آباد'),
(409, 'رباط سنگ'),
(410, 'رشتخوار'),
(411, 'رضويه'),
(412, 'روداب'),
(413, 'ريوش'),
(414, 'سبزوار'),
(415, 'سرخس'),
(416, 'سفيدسنگ'),
(417, 'سلامي'),
(418, 'سلطان آباد'),
(419, 'سنگان'),
(420, 'شادمهر'),
(421, 'شانديز'),
(422, 'ششتمد'),
(423, 'شهرآباد'),
(424, 'شهرزو'),
(425, 'صالح آباد'),
(426, 'طرقبه'),
(427, 'عشق آباد'),
(428, 'فرهادگرد'),
(429, 'فريمان'),
(430, 'فيروزه'),
(431, 'فيض آباد'),
(432, 'قاسم آباد'),
(433, 'قدمگاه'),
(434, 'قلندرآباد'),
(435, 'قوچان'),
(436, 'كاخك'),
(437, 'كاريز'),
(438, 'كاشمر'),
(439, 'كدكن'),
(440, 'كلات'),
(441, 'كندر'),
(442, 'گلمكان'),
(443, 'گناباد'),
(444, 'لطف آباد'),
(445, 'مزدآوند'),
(446, 'مشهد'),
(447, 'مشهدريزه'),
(448, 'ملك آباد'),
(449, 'نشتيفان'),
(450, 'نصرآباد'),
(451, 'نقاب'),
(452, 'نوخندان'),
(453, 'نيشابور'),
(454, 'نيل شهر'),
(455, 'همت آباد'),
(456, 'يونسي'),
(457, 'اسفراين'),
(458, 'ايور'),
(459, 'آشخانه'),
(460, 'بجنورد'),
(461, 'پيش قلعه'),
(462, 'تيتكانلو'),
(463, 'جاجرم'),
(464, 'حصارگرمخان'),
(465, 'درق'),
(466, 'راز'),
(467, 'سنخواست'),
(468, 'شوقان'),
(469, 'شيروان'),
(470, 'صفي آباد'),
(471, 'فاروج'),
(472, 'قاضي'),
(473, 'گرمه'),
(474, 'لوجلي'),
(475, 'اروندكنار'),
(476, 'الوان'),
(477, 'اميديه'),
(478, 'انديمشك'),
(479, 'اهواز'),
(480, 'ايذه'),
(481, 'آبادان'),
(482, 'آغاجاري'),
(483, 'باغ ملك'),
(484, 'بستان'),
(485, 'بندرامام خميني'),
(486, 'بندرماهشهر'),
(487, 'بهبهان'),
(488, 'تركالكي'),
(489, 'جايزان'),
(490, 'جنت مكان'),
(491, 'چغاميش'),
(492, 'چمران'),
(493, 'چوئبده'),
(494, 'حر'),
(495, 'حسينيه'),
(496, 'حمزه'),
(497, 'حميديه'),
(498, 'خرمشهر'),
(499, 'دارخوين'),
(500, 'دزآب'),
(501, 'دزفول'),
(502, 'دهدز'),
(503, 'رامشير'),
(504, 'رامهرمز'),
(505, 'رفيع'),
(506, 'زهره'),
(507, 'سالند'),
(508, 'سردشت'),
(509, 'سماله'),
(510, 'سوسنگرد'),
(511, 'شادگان'),
(512, 'شاوور'),
(513, 'شرافت'),
(514, 'شوش'),
(515, 'شوشتر'),
(516, 'شيبان'),
(517, 'صالح شهر'),
(518, 'صالح مشطط'),
(519, 'صفي آباد'),
(520, 'صيدون'),
(521, 'قلعه تل'),
(522, 'قلعه خواجه'),
(523, 'گتوند'),
(524, 'گوريه'),
(525, 'لالي'),
(526, 'مسجدسليمان'),
(527, 'مشراگه'),
(528, 'مقاومت'),
(529, 'ملاثاني'),
(530, 'ميانرود'),
(531, 'ميداود'),
(532, 'مينوشهر'),
(533, 'ويس'),
(534, 'هفتگل'),
(535, 'هنديجان'),
(536, 'هويزه'),
(537, 'ابهر'),
(538, 'ارمغانخانه'),
(539, 'آب بر'),
(540, 'چورزق'),
(541, 'حلب'),
(542, 'خرمدره'),
(543, 'دندي'),
(544, 'زرين آباد'),
(545, 'زرين رود'),
(546, 'زنجان'),
(547, 'سجاس'),
(548, 'سلطانيه'),
(549, 'سهرورد'),
(550, 'صائين قلعه'),
(551, 'قيدار'),
(552, 'گرماب'),
(553, 'ماه نشان'),
(554, 'هيدج'),
(555, 'اميريه'),
(556, 'ايوانكي'),
(557, 'آرادان'),
(558, 'بسطام'),
(559, 'بيارجمند'),
(560, 'دامغان'),
(561, 'درجزين'),
(562, 'ديباج'),
(563, 'سرخه'),
(564, 'سمنان'),
(565, 'شاهرود'),
(566, 'شهميرزاد'),
(567, 'كلاته خيج'),
(568, 'گرمسار'),
(569, 'مجن'),
(570, 'مهدي شهر'),
(571, 'ميامي'),
(572, 'اديمي'),
(573, 'اسپكه'),
(574, 'ايرانشهر'),
(575, 'بزمان'),
(576, 'بمپور'),
(577, 'بنت'),
(578, 'بنجار'),
(579, 'پيشين'),
(580, 'جالق'),
(581, 'چاه بهار'),
(582, 'خاش'),
(583, 'دوست محمد'),
(584, 'راسك'),
(585, 'زابل'),
(586, 'زابلي'),
(587, 'زاهدان'),
(588, 'زرآباد'),
(589, 'زهك'),
(590, 'سراوان'),
(591, 'سرباز'),
(592, 'سوران'),
(593, 'سيركان'),
(594, 'علي اكبر'),
(595, 'فنوج'),
(596, 'قصرقند'),
(597, 'كنارك'),
(598, 'گشت'),
(599, 'گلمورتي'),
(600, 'محمدان'),
(601, 'محمدآباد'),
(602, 'محمدي'),
(603, 'ميرجاوه'),
(604, 'نصرت آباد'),
(605, 'نگور'),
(606, 'نوك آباد'),
(607, 'نيك شهر'),
(608, 'هيدوج'),
(609, 'اردكان'),
(610, 'ارسنجان'),
(611, 'استهبان'),
(612, 'اسير'),
(613, 'اشكنان'),
(614, 'افزر'),
(615, 'اقليد'),
(616, 'امام شهر'),
(617, 'اوز'),
(618, 'اهل'),
(619, 'ايج'),
(620, 'ايزدخواست'),
(621, 'آباده'),
(622, 'آباده طشك'),
(623, 'باب انار'),
(624, 'بالاده'),
(625, 'بنارويه'),
(626, 'بوانات'),
(627, 'بهمن'),
(628, 'بيرم'),
(629, 'بيضا'),
(630, 'جنت شهر'),
(631, 'جويم'),
(632, 'جهرم'),
(633, 'حاجي آباد'),
(634, 'حسامي'),
(635, 'حسن آباد'),
(636, 'خانه زنيان'),
(637, 'خاوران'),
(638, 'خرامه'),
(639, 'خشت'),
(640, 'خنج'),
(641, 'خور'),
(642, 'خومه زار'),
(643, 'داراب'),
(644, 'داريان'),
(645, 'دبيران'),
(646, 'دژكرد'),
(647, 'دوبرجي'),
(648, 'دوزه'),
(649, 'دهرم'),
(650, 'رامجرد'),
(651, 'رونيز'),
(652, 'زاهدشهر'),
(653, 'زرقان'),
(654, 'سده'),
(655, 'سروستان'),
(656, 'سعادت شهر'),
(657, 'سورمق'),
(658, 'سيدان'),
(659, 'ششده'),
(660, 'شهر جديد صدرا'),
(661, 'شهرپير'),
(662, 'شيراز'),
(663, 'صغاد'),
(664, 'صفاشهر'),
(665, 'علامرودشت'),
(666, 'عمادده'),
(667, 'فدامي'),
(668, 'فراشبند'),
(669, 'فسا'),
(670, 'فيروزآباد'),
(671, 'قادرآباد'),
(672, 'قائميه'),
(673, 'قطب آباد'),
(674, 'قطرويه'),
(675, 'قير'),
(676, 'كارزين'),
(677, 'كازرون'),
(678, 'كامفيروز'),
(679, 'كره اي'),
(680, 'كنارتخته'),
(681, 'كوار'),
(682, 'كوهنجان'),
(683, 'گراش'),
(684, 'گله دار'),
(685, 'لار'),
(686, 'لامرد'),
(687, 'لپوئي'),
(688, 'لطيفي'),
(689, 'مبارك آباد'),
(690, 'مرودشت'),
(691, 'مشكان'),
(692, 'مصيري'),
(693, 'مهر'),
(694, 'ميمند'),
(695, 'نوبندگان'),
(696, 'نوجين'),
(697, 'نودان'),
(698, 'نورآباد'),
(699, 'ني ريز'),
(700, 'وراوي'),
(701, 'هماشهر'),
(702, 'ارداق'),
(703, 'اسفرورين'),
(704, 'اقباليه'),
(705, 'الوند'),
(706, 'آبگرم'),
(707, 'آبيك'),
(708, 'آوج'),
(709, 'بوئين زهرا'),
(710, 'بيدستان'),
(711, 'تاكستان'),
(712, 'خاكعلي'),
(713, 'خرمدشت'),
(714, 'دانسفهان'),
(715, 'رازميان'),
(716, 'سگزآباد'),
(717, 'سيردان'),
(718, 'شال'),
(719, 'شريفيه'),
(720, 'ضياءآباد'),
(721, 'قزوين'),
(722, 'كوهين'),
(723, 'محمديه'),
(724, 'محمودآبادنمونه'),
(725, 'معلم كلايه'),
(726, 'نرجه'),
(727, 'جعفريه'),
(728, 'دستجرد'),
(729, 'سلفچگان'),
(730, 'قم'),
(731, 'قنوات'),
(732, 'كهك'),
(733, 'آرمرده'),
(734, 'بابارشاني'),
(735, 'بانه'),
(736, 'بلبان آباد'),
(737, 'بوئين سفلي'),
(738, 'بيجار'),
(739, 'چناره'),
(740, 'دزج'),
(741, 'دلبران'),
(742, 'دهگلان'),
(743, 'ديواندره'),
(744, 'زرينه'),
(745, 'سروآباد'),
(746, 'سريش آباد'),
(747, 'سقز'),
(748, 'سنندج'),
(749, 'شويشه'),
(750, 'صاحب'),
(751, 'قروه'),
(752, 'كامياران'),
(753, 'كاني دينار'),
(754, 'كاني سور'),
(755, 'مريوان'),
(756, 'موچش'),
(757, 'ياسوكند'),
(758, 'اختيارآباد'),
(759, 'ارزوئيه'),
(760, 'امين شهر'),
(761, 'انار'),
(762, 'اندوهجرد'),
(763, 'باغين'),
(764, 'بافت'),
(765, 'بردسير'),
(766, 'بروات'),
(767, 'بزنجان'),
(768, 'بم'),
(769, 'بهرمان'),
(770, 'پاريز'),
(771, 'جبالبارز'),
(772, 'جوپار'),
(773, 'جوزم'),
(774, 'جيرفت'),
(775, 'چترود'),
(776, 'خاتون آباد'),
(777, 'خانوك'),
(778, 'خورسند'),
(779, 'درب بهشت'),
(780, 'دوساري'),
(781, 'دهج'),
(782, 'رابر'),
(783, 'راور'),
(784, 'راين'),
(785, 'رفسنجان'),
(786, 'رودبار'),
(787, 'ريحان شهر'),
(788, 'زرند'),
(789, 'زنگي آباد'),
(790, 'زيدآباد'),
(791, 'سرچشمه'),
(792, 'سيرجان'),
(793, 'شهداد'),
(794, 'شهربابك'),
(795, 'صفائيه'),
(796, 'عنبرآباد'),
(797, 'فارياب'),
(798, 'فهرج'),
(799, 'قلعه گنج'),
(800, 'كاظم آباد'),
(801, 'كرمان'),
(802, 'كشكوئيه'),
(803, 'كوهبنان'),
(804, 'كهنوج'),
(805, 'كيانشهر'),
(806, 'گلباف'),
(807, 'گلزار'),
(808, 'لاله زار'),
(809, 'ماهان'),
(810, 'محمدآباد'),
(811, 'محي آباد'),
(812, 'مردهك'),
(813, 'منوجان'),
(814, 'نجف شهر'),
(815, 'نرماشير'),
(816, 'نظام شهر'),
(817, 'نگار'),
(818, 'نودژ'),
(819, 'هجدك'),
(820, 'هماشهر'),
(821, 'يزدان شهر'),
(822, 'ازگله'),
(823, 'اسلام آبادغرب'),
(824, 'باينگان'),
(825, 'بيستون'),
(826, 'پاوه'),
(827, 'تازه آباد'),
(828, 'جوانرود'),
(829, 'حميل'),
(830, 'رباط'),
(831, 'روانسر'),
(832, 'سرپل ذهاب'),
(833, 'سرمست'),
(834, 'سطر'),
(835, 'سنقر'),
(836, 'سومار'),
(837, 'شاهو'),
(838, 'صحنه'),
(839, 'قصرشيرين'),
(840, 'كرمانشاه'),
(841, 'كرندغرب'),
(842, 'كنگاور'),
(843, 'كوزران'),
(844, 'گهواره'),
(845, 'گيلانغرب'),
(846, 'ميان راهان'),
(847, 'نودشه'),
(848, 'نوسود'),
(849, 'هرسين'),
(850, 'هلشي'),
(851, 'باشت'),
(852, 'پاتاوه'),
(853, 'چرام'),
(854, 'چيتاب'),
(855, 'دوگنبدان'),
(856, 'دهدشت'),
(857, 'ديشموك'),
(858, 'سوق'),
(859, 'سي سخت'),
(860, 'قلعه رئيسي'),
(861, 'گراب سفلي'),
(862, 'لنده'),
(863, 'ليكك'),
(864, 'مادوان'),
(865, 'مارگون'),
(866, 'ياسوج'),
(867, 'انبارآلوم'),
(868, 'اينچه برون'),
(869, 'آزادشهر'),
(870, 'آق قلا'),
(871, 'بندرگز'),
(872, 'تركمن'),
(873, 'جلين'),
(874, 'خان ببين'),
(875, 'دلند'),
(876, 'راميان'),
(877, 'سرخنكلاته'),
(878, 'سيمين شهر'),
(879, 'علي آباد'),
(880, 'فاضل آباد'),
(881, 'كردكوي'),
(882, 'كلاله'),
(883, 'گاليكش'),
(884, 'گرگان'),
(885, 'گميش تپه'),
(886, 'گنبد كاووس'),
(887, 'مراوه تپه'),
(888, 'مينودشت'),
(889, 'نگين شهر'),
(890, 'نوده خاندوز'),
(891, 'نوكنده'),
(892, 'احمدسرگوراب'),
(893, 'اسالم'),
(894, 'اطاقور'),
(895, 'املش'),
(896, 'آستارا'),
(897, 'آستانه اشرفيه'),
(898, 'بازارجمعه'),
(899, 'بره سر'),
(900, 'بندرانزلي'),
(901, 'پره سر'),
(902, 'توتكابن'),
(903, 'جيرنده'),
(904, 'چابكسر'),
(905, 'چاف وچمخاله'),
(906, 'چوبر'),
(907, 'حويق'),
(908, 'خشكبيجار'),
(909, 'خمام'),
(910, 'ديلمان'),
(911, 'رانكوه'),
(912, 'رحيم آباد'),
(913, 'رستم آباد'),
(914, 'رشت'),
(915, 'رضوانشهر'),
(916, 'رودبار'),
(917, 'رودبنه'),
(918, 'رودسر'),
(919, 'سنگر'),
(920, 'سياهكل'),
(921, 'شفت'),
(922, 'شلمان'),
(923, 'صومعه سرا'),
(924, 'فومن'),
(925, 'كلاچاي'),
(926, 'كوچصفهان'),
(927, 'كومله'),
(928, 'كياشهر'),
(929, 'گوراب زرميخ'),
(930, 'لاهيجان'),
(931, 'لشت نشاء'),
(932, 'لنگرود'),
(933, 'لوشان'),
(934, 'لولمان'),
(935, 'لوندويل'),
(936, 'ليسار'),
(937, 'ماسال'),
(938, 'ماسوله'),
(939, 'مرجقل'),
(940, 'منجيل'),
(941, 'واجارگاه'),
(942, 'هشتپر'),
(943, 'ازنا'),
(944, 'اشترينان'),
(945, 'الشتر'),
(946, 'اليگودرز'),
(947, 'بروجرد'),
(948, 'پلدختر'),
(949, 'چالانچولان'),
(950, 'چغلوندي'),
(951, 'چقابل'),
(952, 'خرم آباد'),
(953, 'درب گنبد'),
(954, 'دورود'),
(955, 'زاغه'),
(956, 'سپيددشت'),
(957, 'سراب دوره'),
(958, 'شول آباد'),
(959, 'فيروزآباد'),
(960, 'كوناني'),
(961, 'كوهدشت'),
(962, 'گراب'),
(963, 'معمولان'),
(964, 'مؤمن آباد'),
(965, 'نورآباد'),
(966, 'ويسيان'),
(967, 'هفت چشمه'),
(968, 'اميركلا'),
(969, 'ايزدشهر'),
(970, 'آلاشت'),
(971, 'آمل'),
(972, 'بابل'),
(973, 'بابلسر'),
(974, 'بلده'),
(975, 'بهشهر'),
(976, 'بهنمير'),
(977, 'پل سفيد'),
(978, 'پول'),
(979, 'تنكابن'),
(980, 'جويبار'),
(981, 'چالوس'),
(982, 'چمستان'),
(983, 'خرم آباد'),
(984, 'خليل شهر'),
(985, 'خوش رودپي'),
(986, 'دابودشت'),
(987, 'رامسر'),
(988, 'رستمكلا'),
(989, 'رويان'),
(990, 'رينه'),
(991, 'زرگر محله'),
(992, 'زيرآب'),
(993, 'ساري'),
(994, 'سرخرود'),
(995, 'سلمان شهر'),
(996, 'سورك'),
(997, 'شيرگاه'),
(998, 'شيرود'),
(999, 'عباس آباد'),
(1000, 'فريدونكنار'),
(1001, 'فريم'),
(1002, 'قائم شهر'),
(1003, 'كتالم وسادات شهر'),
(1004, 'كلارآباد'),
(1005, 'كلاردشت'),
(1006, 'كله بست'),
(1007, 'كوهي خيل'),
(1008, 'كياسر'),
(1009, 'كياكلا'),
(1010, 'گتاب'),
(1011, 'گزنك'),
(1012, 'گلوگاه'),
(1013, 'محمودآباد'),
(1014, 'مرزن آباد'),
(1015, 'مرزيكلا'),
(1016, 'نشتارود'),
(1017, 'نكا'),
(1018, 'نور'),
(1019, 'نوشهر'),
(1020, 'اراك'),
(1021, 'آستانه'),
(1022, 'آشتيان'),
(1023, 'پرندك'),
(1024, 'تفرش'),
(1025, 'توره'),
(1026, 'جاورسيان'),
(1027, 'خشكرود'),
(1028, 'خمين'),
(1029, 'خنداب'),
(1030, 'داودآباد'),
(1031, 'دليجان'),
(1032, 'رازقان'),
(1033, 'زاويه'),
(1034, 'ساروق'),
(1035, 'ساوه'),
(1036, 'سنجان'),
(1037, 'شازند'),
(1038, 'شهرجديدمهاجران'),
(1039, 'غرق آباد'),
(1040, 'فرمهين'),
(1041, 'قورچي باشي'),
(1042, 'كرهرود'),
(1043, 'كميجان'),
(1044, 'مأمونيه'),
(1045, 'محلات'),
(1046, 'ميلاجرد'),
(1047, 'نراق'),
(1048, 'نوبران'),
(1049, 'نيمور'),
(1050, 'هندودر'),
(1051, 'ابوموسي'),
(1052, 'بستك'),
(1053, 'بندرجاسك'),
(1054, 'بندرچارك'),
(1055, 'بندرعباس'),
(1056, 'بندرلنگه'),
(1057, 'بيكاه'),
(1058, 'پارسيان'),
(1059, 'تخت'),
(1060, 'جناح'),
(1061, 'حاجي آباد'),
(1062, 'خمير'),
(1063, 'درگهان'),
(1064, 'دهبارز'),
(1065, 'رويدر'),
(1066, 'زيارتعلي'),
(1067, 'سردشت بشاگرد'),
(1068, 'سرگز'),
(1069, 'سندرك'),
(1070, 'سوزا'),
(1071, 'سيريك'),
(1072, 'فارغان'),
(1073, 'فين'),
(1074, 'قشم'),
(1075, 'قلعه قاضي'),
(1076, 'كنگ'),
(1077, 'كوشكنار'),
(1078, 'كيش'),
(1079, 'گوهران'),
(1080, 'ميناب'),
(1081, 'هرمز'),
(1082, 'هشتبندي'),
(1083, 'ازندريان'),
(1084, 'اسدآباد'),
(1085, 'برزول'),
(1086, 'بهار'),
(1087, 'تويسركان'),
(1088, 'جورقان'),
(1089, 'جوكار'),
(1090, 'دمق'),
(1091, 'رزن'),
(1092, 'زنگنه'),
(1093, 'سامن'),
(1094, 'سركان'),
(1095, 'شيرين سو'),
(1096, 'صالح آباد'),
(1097, 'فامنين'),
(1098, 'فرسفج'),
(1099, 'فيروزان'),
(1100, 'قروه در جزين'),
(1101, 'قهاوند'),
(1102, 'كبودرآهنگ'),
(1103, 'گل تپه'),
(1104, 'گيان'),
(1105, 'لالجين'),
(1106, 'مريانج'),
(1107, 'ملاير'),
(1108, 'نهاوند'),
(1109, 'همدان'),
(1110, 'ابركوه'),
(1111, 'احمدآباد'),
(1112, 'اردكان'),
(1113, 'اشكذر'),
(1114, 'بافق'),
(1115, 'بفروئيه'),
(1116, 'بهاباد'),
(1117, 'تفت'),
(1118, 'حميديا'),
(1119, 'خضرآباد'),
(1120, 'ديهوك'),
(1121, 'زارچ'),
(1122, 'شاهديه'),
(1123, 'طبس'),
(1124, 'عشق آباد'),
(1125, 'عقدا'),
(1126, 'مروست'),
(1127, 'مهردشت'),
(1128, 'مهريز'),
(1129, 'ميبد'),
(1130, 'ندوشن'),
(1131, 'نير'),
(1132, 'هرات'),
(1133, 'يزد');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `idcomment` int(11) NOT NULL,
  `comment_text` text,
  `comment_view` tinyint(4) DEFAULT NULL,
  `company_idcompany` int(11) NOT NULL,
  `imploye_idusers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`idcomment`, `comment_text`, `comment_view`, `company_idcompany`, `imploye_idusers`) VALUES
(1, 'salam', 0, 9, 13),
(4, 'Kheeeily khobi tooooooo', 0, 9, 13),
(5, 'heeeeeey', 1, 9, 13),
(6, 'midonam nemibini!', 0, 9, 12),
(7, 'ino mibini', 1, 9, 12),
(8, 'ino to nemibini', 0, 9, 13);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `idcompany` int(11) NOT NULL,
  `company_name` varchar(45) DEFAULT NULL,
  `users_idusers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`idcompany`, `company_name`, `users_idusers`) VALUES
(8, 'Extreme', 9),
(9, 'Are', 25);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `idcountry` int(11) NOT NULL,
  `country_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`idcountry`, `country_name`) VALUES
(1, 'ایران');

-- --------------------------------------------------------

--
-- Table structure for table `degry`
--

CREATE TABLE `degry` (
  `iddegry` int(11) NOT NULL,
  `degry_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `degry`
--

INSERT INTO `degry` (`iddegry`, `degry_name`) VALUES
(1, 'سیکل'),
(2, 'دیپلم'),
(3, 'پیش دانشگاهی'),
(4, 'فوق دیپلم'),
(5, 'لیسانس'),
(6, 'کارشناسی'),
(7, 'دکتری'),
(8, 'پرفوسور');

-- --------------------------------------------------------

--
-- Table structure for table `field`
--

CREATE TABLE `field` (
  `idfield` int(11) NOT NULL,
  `field_name` varchar(45) DEFAULT NULL,
  `company_idcompany` int(11) NOT NULL,
  `company_users_idusers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field`
--

INSERT INTO `field` (`idfield`, `field_name`, `company_idcompany`, `company_users_idusers`) VALUES
(1, 'IT , Develop , Client', 8, 9),
(2, 'همه چی', 9, 25);

-- --------------------------------------------------------

--
-- Table structure for table `hoby`
--

CREATE TABLE `hoby` (
  `idhoby` int(11) NOT NULL,
  `hoby_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hoby`
--

INSERT INTO `hoby` (`idhoby`, `hoby_name`) VALUES
(42, 'علاقه'),
(43, 'تو'),
(44, 'ندارم'),
(45, 'بازی'),
(46, 'بازی');

-- --------------------------------------------------------

--
-- Table structure for table `imploye`
--

CREATE TABLE `imploye` (
  `imploye_idusers` int(11) NOT NULL,
  `users_idusers` int(11) NOT NULL,
  `imploye_fullname` varchar(100) DEFAULT NULL,
  `imploye_gen` tinyint(4) DEFAULT NULL,
  `imploye_bdate` date DEFAULT NULL,
  `imploye_desc` text,
  `country_idcountry` int(11) NOT NULL,
  `degry_iddegry` int(11) NOT NULL,
  `degry_avg` float NOT NULL,
  `city_id` int(11) NOT NULL,
  `bcity_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imploye`
--

INSERT INTO `imploye` (`imploye_idusers`, `users_idusers`, `imploye_fullname`, `imploye_gen`, `imploye_bdate`, `imploye_desc`, `country_idcountry`, `degry_iddegry`, `degry_avg`, `city_id`, `bcity_id`) VALUES
(12, 23, 'ماهان شوقی نژاد', 0, '1999-10-10', 'خیلی خوبم', 1, 3, 16, 931, 897),
(13, 24, 'علی پاک', 0, '2019-06-20', 'آررره پسرررررررر', 1, 2, 15, 931, 914);

-- --------------------------------------------------------

--
-- Table structure for table `imploye_has_hoby`
--

CREATE TABLE `imploye_has_hoby` (
  `imploye_idusers` int(11) NOT NULL,
  `hoby_idhoby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imploye_has_hoby`
--

INSERT INTO `imploye_has_hoby` (`imploye_idusers`, `hoby_idhoby`) VALUES
(12, 42),
(12, 43),
(12, 44),
(12, 45),
(13, 45);

-- --------------------------------------------------------

--
-- Table structure for table `imploye_has_skill`
--

CREATE TABLE `imploye_has_skill` (
  `imploye_idusers` int(11) NOT NULL,
  `skill_idskill` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imploye_has_skill`
--

INSERT INTO `imploye_has_skill` (`imploye_idusers`, `skill_idskill`) VALUES
(12, 34),
(12, 35),
(12, 36),
(12, 38),
(13, 37),
(13, 39);

-- --------------------------------------------------------

--
-- Table structure for table `jobxp`
--

CREATE TABLE `jobxp` (
  `idjobxp` int(11) NOT NULL,
  `jobxp_cname` varchar(45) DEFAULT NULL,
  `jobxp_startdate` date DEFAULT NULL,
  `jobxp_enddate` date DEFAULT NULL,
  `jobxp_reason` varchar(45) DEFAULT NULL,
  `imploye_idusers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobxp`
--

INSERT INTO `jobxp` (`idjobxp`, `jobxp_cname`, `jobxp_startdate`, `jobxp_enddate`, `jobxp_reason`, `imploye_idusers`) VALUES
(14, 'اینجا', '2019-10-01', '2019-10-07', 'به یه دلیلی', 12),
(15, 'asd', '2019-10-10', '2019-10-13', 'dasdasd', 12),
(16, 'asdasddddd', '2019-10-02', '2019-10-11', 'asdada', 12);

-- --------------------------------------------------------

--
-- Table structure for table `requset`
--

CREATE TABLE `requset` (
  `idrequset` int(11) NOT NULL,
  `company_idcompany` int(11) NOT NULL,
  `imploye_idusers` int(11) NOT NULL,
  `result` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `requset`
--

INSERT INTO `requset` (`idrequset`, `company_idcompany`, `imploye_idusers`, `result`) VALUES
(1, 8, 12, 0),
(2, 9, 12, 0),
(24, 9, 13, 0),
(25, 8, 13, 0);

-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

CREATE TABLE `skill` (
  `idskill` int(11) NOT NULL,
  `skill_name` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `skill`
--

INSERT INTO `skill` (`idskill`, `skill_name`) VALUES
(34, 'خودم'),
(35, 'مهارت'),
(36, 'دارم'),
(37, 'سخت افزار'),
(38, 'کشک'),
(39, 'یه کاری');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idusers` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `pnumber` varchar(45) DEFAULT NULL,
  `flag` int(1) DEFAULT NULL,
  `signither` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idusers`, `email`, `password`, `pnumber`, `flag`, `signither`) VALUES
(9, '123@yahoo.com', 'ee1b2566b4ca320c2d03cfd21eeeb4d0', '09017477274', 1, ''),
(23, 'mahan_1414@yahoo.com', '96388c409abe42c8fc46bc8ea157f57b', '09017477274', 0, ''),
(24, 'ali@yahoo.com', '554914b8c68885b069b187031aba8ba6', '09871564287', 0, ''),
(25, 'are@yahoo.com', '746a85f732c7511ea8babbadcd917d7c', '09376859573', 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`ipuser`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`idcomment`,`company_idcompany`,`imploye_idusers`),
  ADD KEY `fk_comment_company1_idx` (`company_idcompany`),
  ADD KEY `fk_comment_imploye1_idx` (`imploye_idusers`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`idcompany`,`users_idusers`),
  ADD KEY `fk_company_users1_idx` (`users_idusers`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`idcountry`);

--
-- Indexes for table `degry`
--
ALTER TABLE `degry`
  ADD PRIMARY KEY (`iddegry`);

--
-- Indexes for table `field`
--
ALTER TABLE `field`
  ADD PRIMARY KEY (`idfield`,`company_idcompany`,`company_users_idusers`),
  ADD KEY `fk_field_company1_idx` (`company_idcompany`,`company_users_idusers`);

--
-- Indexes for table `hoby`
--
ALTER TABLE `hoby`
  ADD PRIMARY KEY (`idhoby`);

--
-- Indexes for table `imploye`
--
ALTER TABLE `imploye`
  ADD PRIMARY KEY (`imploye_idusers`,`users_idusers`,`country_idcountry`,`degry_iddegry`,`city_id`,`bcity_id`) USING BTREE,
  ADD KEY `fk_imploye_country1_idx` (`country_idcountry`),
  ADD KEY `fk_imploye_degry1_idx` (`degry_iddegry`),
  ADD KEY `fk_imploye_city1_idx` (`city_id`),
  ADD KEY `fk_idusers_users_idx` (`users_idusers`) USING BTREE,
  ADD KEY `fk_bcity_city` (`bcity_id`);

--
-- Indexes for table `imploye_has_hoby`
--
ALTER TABLE `imploye_has_hoby`
  ADD PRIMARY KEY (`imploye_idusers`,`hoby_idhoby`),
  ADD KEY `fk_imploye_has_hoby_hoby1_idx` (`hoby_idhoby`),
  ADD KEY `fk_imploye_has_hoby_imploye1_idx` (`imploye_idusers`);

--
-- Indexes for table `imploye_has_skill`
--
ALTER TABLE `imploye_has_skill`
  ADD PRIMARY KEY (`imploye_idusers`,`skill_idskill`),
  ADD KEY `fk_imploye_has_skill_skill1_idx` (`skill_idskill`),
  ADD KEY `fk_imploye_has_skill_imploye1_idx` (`imploye_idusers`);

--
-- Indexes for table `jobxp`
--
ALTER TABLE `jobxp`
  ADD PRIMARY KEY (`idjobxp`,`imploye_idusers`),
  ADD KEY `fk_jobxp_imploye1_idx` (`imploye_idusers`);

--
-- Indexes for table `requset`
--
ALTER TABLE `requset`
  ADD PRIMARY KEY (`idrequset`,`company_idcompany`,`imploye_idusers`),
  ADD KEY `fk_company_has_imploye_imploye1_idx` (`imploye_idusers`),
  ADD KEY `fk_company_has_imploye_company1_idx` (`company_idcompany`);

--
-- Indexes for table `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`idskill`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idusers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1134;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `idcomment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `idcompany` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `idcountry` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `degry`
--
ALTER TABLE `degry`
  MODIFY `iddegry` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `field`
--
ALTER TABLE `field`
  MODIFY `idfield` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hoby`
--
ALTER TABLE `hoby`
  MODIFY `idhoby` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `imploye`
--
ALTER TABLE `imploye`
  MODIFY `imploye_idusers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `jobxp`
--
ALTER TABLE `jobxp`
  MODIFY `idjobxp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `requset`
--
ALTER TABLE `requset`
  MODIFY `idrequset` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `skill`
--
ALTER TABLE `skill`
  MODIFY `idskill` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idusers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_comment_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_comment_imploye1` FOREIGN KEY (`imploye_idusers`) REFERENCES `imploye` (`imploye_idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `fk_company_users1` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `field`
--
ALTER TABLE `field`
  ADD CONSTRAINT `fk_field_company1` FOREIGN KEY (`company_idcompany`,`company_users_idusers`) REFERENCES `company` (`idcompany`, `users_idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `imploye`
--
ALTER TABLE `imploye`
  ADD CONSTRAINT `fk_bcity_city` FOREIGN KEY (`bcity_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_imploye_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_imploye_country1` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_imploye_degry1` FOREIGN KEY (`degry_iddegry`) REFERENCES `degry` (`iddegry`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_login` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `imploye_has_hoby`
--
ALTER TABLE `imploye_has_hoby`
  ADD CONSTRAINT `fk_imploye_has_hoby_hoby1` FOREIGN KEY (`hoby_idhoby`) REFERENCES `hoby` (`idhoby`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_imploye_has_hoby_imploye1` FOREIGN KEY (`imploye_idusers`) REFERENCES `imploye` (`imploye_idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `imploye_has_skill`
--
ALTER TABLE `imploye_has_skill`
  ADD CONSTRAINT `fk_imploye_has_skill_imploye1` FOREIGN KEY (`imploye_idusers`) REFERENCES `imploye` (`imploye_idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_imploye_has_skill_skill1` FOREIGN KEY (`skill_idskill`) REFERENCES `skill` (`idskill`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jobxp`
--
ALTER TABLE `jobxp`
  ADD CONSTRAINT `fk_jobxp_imploye1` FOREIGN KEY (`imploye_idusers`) REFERENCES `imploye` (`imploye_idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `requset`
--
ALTER TABLE `requset`
  ADD CONSTRAINT `fk_company_has_imploye_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_company_has_imploye_imploye1` FOREIGN KEY (`imploye_idusers`) REFERENCES `imploye` (`imploye_idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
