<?php
    require_once("../include/set.php");

    # check user company or imployer
    if (isset($_SESSION["login-imploye"])) {
      $email = $_SESSION["login-imploye"];
      include"dashboard.controller.php";
    }
    elseif (isset($_SESSION["login-company"])) {
      $email = $_SESSION["login-company"];

      $text = $_GET["textsearch"];

      # check user select a filter or not
      if (isset($_GET["typefilter"])) {
        $type_filter = $_GET["typefilter"];
      }
      else {
        $type_filter = 4;
      }

      # set result for filters
      switch ($type_filter) {
        case 0:
        $result = mysqli_fetch_all(querySQL("select skill_name from skill inner join imploye_has_skill
        on skill_idskill = idskill
        inner join imploye
        on imploye.imploye_idusers = imploye_has_skill.imploye_idusers
        where skill_name like '%$text%'"));
        break;
        case 1:
        $result = mysqli_fetch_all(querySQL("select imploye_fullname from imploye
        where imploye_fullname like '%$text%'"));
        break;
        case 2:
        $result = mysqli_fetch_all(querySQL("select name_city from city
        inner join imploye
        on city.id = imploye.city_id
        where name_city like '%$text%'"));
        break;
        case 3:
        $result = mysqli_fetch_all(querySQL("select degry_name from degry
        inner join imploye
        on degry.iddegry = imploye.degry_iddegry
        where degry_name like '%$text%'"));
        break;
        default:
        $result = mysqli_fetch_all(querySQL("select imploye_fullname from imploye
        where imploye_fullname like '%$text%'"));
        break;
      }
      include"../view/search.company.view.php";
    }
    else {
      header('Location: ../index.php');
    }
?>
