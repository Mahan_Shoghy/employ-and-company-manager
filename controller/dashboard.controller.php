<?php
    require_once("../include/set.php");

    # check user company or imployer
    if (isset($_SESSION["login-imploye"])) {
      $email = $_SESSION["login-imploye"];
      include"../view/dashboard.imploye.view.php";
    }
    elseif (isset($_SESSION["login-company"])) {
      $email = $_SESSION["login-company"];
      include"../view/dashboard.company.view.php";
    }
    else {
      header('Location: ../index.php');
    }
?>
