<?php
    require_once("../include/set.php");

    # check user company or imployer
    if (isset($_SESSION["login-imploye"])) {
      if (isset($_POST['edit'])) {
        $message_error = editImployer();
      }
      include"../view/edit.imploye.view.php";
    }
    elseif (isset($_SESSION["login-company"])) {
      if (isset($_POST['edit'])) {
        $message_error = editCompany();
      }
      include"../view/edit.company.view.php";
    }
    else {
      header('Location: ../index.php');
    }
?>
