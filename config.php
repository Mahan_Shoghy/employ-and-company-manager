<?php

require_once("../protected/dbconnection.config.php");

# func for throw exception
function throwException($message = null ,$code = null ,$file = null ,$line = null)
{
  throw new Exception($message, $code, $file, $line);
}

try {
 $connection = mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME) or throwException("error");
}
catch (Exception $e) {
 error_log("Server connection error", 3 , "log_error\error.log");
}
finally {
 $error = "<h1>Somthing wrong. Please try again later</h1>";
 return $error;
}
