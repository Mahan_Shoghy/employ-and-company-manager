<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
  require_once "../model/model.php";
  require_once "../protected/dbconnection.config.php";
  require_once "cookie.inc.php";
  require_once "func.inc.php";

  blockcheck();
?>