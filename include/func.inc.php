<?php

# function for validate some inputs
function validate($str) {
  return trim(htmlspecialchars($str));
}

# function for search text in a value and if true show your message
function boolSearchInInputs($regex,$value,$msg)
{
  if (preg_match('/'.$regex.'/',$value)) {
    $message_error = $msg;
    return $message_error;
  }
}

# function for run sql easy
function querySQL($sql)
{
  $connection = mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);
  mysqli_query($connection, "SET NAMES utf8");
  $spc = mysqli_query($connection,$sql); // returns a mysql_result object
  mysqli_close($connection);
  return $spc;
}

# better than default set cookie
function mySetCookie($name,$value,$expirytime)
{
  setcookie($name,$value,$expirytime,null,null,null,true);
}

# check inputs if they empty show message
function checkEmptyInputs($value,$err_msg)
{
  if (empty($value)) {
    if (isset($err_msg)) {
      return $err_msg;
    }
  }
}

function genRandomString($count)
{
  $characters = '0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()_+';
    $randomString = '';
    for ($i = 0; $i < $count; $i++) {
        $randomString .= $characters[rand(0,47)];
    }
    return $randomString;
}

# return reCAPTCHA
function reCAPTCHA()
{
  return '<div class="g-recaptcha" name="g-recaptcha-response" data-sitekey="6LdbH7wUAAAAALECLUk0WvFupTcA5hN4F_2KioNN"></div>';  
}

# return a picture of random captcha
function RandomCaptcha()
{
  return '
  <div class="form-group">
    <img src="../include/captcha.php">
    <input type="text" name="captcha" class="form-control" placeholder="Enter Above Text" required>
  </div>';
}

# return a simple mathc question for captcha
function ShowNumber()
{
  $number = array(1,2,3,4,5,6,7,8,9);
  $num1 = $number[array_rand($number)];
  $num2 = $number[array_rand($number)];
  $_SESSION['randomcaptcha'] = $num1 + $num2;
  return '
  <div class="form-group">
    <div class="form-row">
      <div class="col">
        <input type="text" class="form-control" placeholder="'.$num1.' + '.$num2.'" disabled>
      </div>
      <div class="col">
        <input type="text" name="randomcaptcha" class="form-control" placeholder="Result" required>
      </div>
    </div>
  </div>';
}

# generait captcha
function genCaptcha()
{
  $randomselect = array(1,2,3);
  $randomselect = array_rand($randomselect);
  if ($randomselect === 0){
    return ShowNumber();
  }
  elseif ($randomselect === 1){
    return RandomCaptcha();
  }
  elseif ($randomselect === 2){
    return reCAPTCHA();
  }
}

# check random captcha
function CheckRanCaptcha()
{
  if (isset($_POST['captcha']) && !empty($_POST['captcha'])) {
    if ($_POST['captcha'] != $_SESSION['captcha_text']) {
        return "*Invaild Captcha Please try again";
    }
  }
}

# check picture captcha
function CheckPicCaptcha()
{
  if (isset($_POST['randomcaptcha']) && !empty($_POST['randomcaptcha'])) {
    if ($_POST['randomcaptcha'] != $_SESSION['randomcaptcha']) {
      return "*Invaild Captcha Please try again";
    }
  }
}

# check recaptcha
function CheckReCaptcha()
{
  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
  {
        $secret = '6LdbH7wUAAAAAFph_GWv3NkIS5w7gIenN4ZE1N68';
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success)
        {
          return '*Robot verification failed, please try again';
        }
   }
}

# check all captchas
function CheckCaptcha()
{
  if (!empty(CheckRanCaptcha())){
    return CheckRanCaptcha();
  }
  elseif (!empty(CheckPicCaptcha())){
    return CheckPicCaptcha();
  }
  elseif (!empty(CheckReCaptcha())){
    return CheckReCaptcha();
  }
}

# check empty inputs of company
function CheckEmptyInputCompany()
{
  $source = array("Please enter Email"=>$_POST["email"],"Please enter Company Name"=>$_POST["companyname"],"Please enter Passeord"=>$_POST["password"],"Please enter Field"=>$_POST["field"],"Please enter Phone Number"=>$_POST["pnumber"]);
   foreach($source as $msg => $variable){
    return checkEmptyInputs($variable,$msg);
   }
}

# check empty inputs of imploye
function CheckEmptyInputImploye()
{
  $source = array("Please enter Email"=>$_POST["email"],"Please enter Company Name"=>$_POST["companyname"],"Please enter Passeord"=>$_POST["password"],"Please enter Field"=>$_POST["field"],"Please enter Phone Number"=>$_POST["pnumber"]);
   foreach($source as $msg => $variable){
    return checkEmptyInputs($variable,$msg);
   }
}

# validate age
function getAge($date) {
    return intval(date('Y', time() - strtotime($date))) - 1970;
}

# check password not be contain with any another inputs
function PassContain()
{
  $source = array("Email" => $_POST["email"] , "Companyname" => $_POST["companyname"]);
    foreach ($source as $name => $value) {
      return boolSearchInInputs($value,$_POST["password"],"*Password not be contain with your $name");
    }
}

# check password with regex
function CheckPass()
{
  $regex_pass = ('/^(?=\P{Ll}*\p{Ll})(?=\P{Lu}*\p{Lu})(?=\P{N}*\p{N})(?=[\p{L}\p{N}]*[^\p{L}\p{N}])[\s\S]{8,}$/'); // regex for password
    if (!preg_match($regex_pass, $_POST['password'])) {
      return "*Invaild Password ( Contain letters ( upper,lower ) , Number , Special Char )";
    }
}

# validate email
function ValidateEmail()
{
  $email = validate($_POST['email']);
  if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
    return "*Invalid Email ( Contain Letters , Numbers , @ & .domain )";
  }
}

# set session for login
function SessionSet($email)
{
  # check user is imploye or company
  if (UserFlag($email) == 0) {
    $_SESSION["login-imploye"] = $email;
  }
  elseif (UserFlag($email) == 1) {
    $_SESSION["login-company"] = $email;
  }
}

# function for validate all company signup inputs
function valdatecompanysignup()
{
   # check captcha
   if(CheckCaptcha())
     $message_error[] = CheckCaptcha();

   # check inputs not be empty
   if(CheckEmptyInputCompany())
     $message_error[] = CheckEmptyInputCompany();

   # check password not be contain with any another inputs
   if(PassContain())
     $message_error[] = PassContain();

   # check password with regex
   if(CheckPass())
     $message_error[] = CheckPass();   
    
   # validate email
   if(ValidateEmail())
     $message_error[] = ValidateEmail();   
    
   return $message_error;
}

# function for validate all imploy signup inputs
function valdateimploysignup()
{
  # check captcha
  if(CheckCaptcha())
    $message_error[] = CheckCaptcha();

  # check in inputs to not be empty
  if(CheckEmptyInputImploye())
    $message_error[] = CheckEmptyInputImploye();

  # validate email
  if(ValidateEmail())
    $message_error[] = ValidateEmail();   

  # check city and birth city
  if(in_array($_POST["city"],Citys()))
    $message_error[] = "*In City .Please select somthing between values";   
  if(in_array($_POST["birthcity"],Citys()))
    $message_error[] = "*In Birth City .Please select somthing between values";   

  # check country
  if(in_array($_POST["country"],Country()))
    $message_error[] = "*In Country .Please select somthing between values";   

  # check age
  if(getAge($_POST["birthdate"]) <= 18)
    $message_error[] = "*Sorry you are to young";   

  # check degry
  if(in_array($_POST["lastdegry"],Degrys()))
    $message_error[] = "*In Degry .Please select somthing between values";   

  return $message_error;
}

function RegisterCompany()
{

  $password = $_POST["password"];
  $password = md5($password);
  $source = array("Companyname" => $_POST["companyname"],"Phonenumber" => $_POST["pnumber"],"Email" => $_POST["email"],"Password" => $password,"Field" => $_POST["field"]);

  # check values exist in database or not
  if(NameCheckCompany($source['Companyname']) === 1) {
    return $message_error[] = "*This Company Name already taken. Please enter another one";
  }
  if(EmailCheck($source['Email']) === 1) {
    return $message_error[] = "*This Email in used. Please enter another one";
  }

  # Insert for register company
  InsertUsers($source['Email'],$source['Password'],$source['Phonenumber'],1);
  InsertCompany($source['Companyname'],$source['Email'],$source['Field']);

  header('Location: login.controller.php');
}

# do all company signup works
function companysignup()
{
  # check we have an error or not
  if(valdatecompanysignup()[0] !== null) {
      return valdatecompanysignup();
  }

  RegisterCompany();
}

# do all imploys signup works
function imployssignup()
{
  if(valdateimploysignup()[0] !== null) {
    return valdateimploysignup();
  }

  # get some value of inputs
  $name = $_POST['name']." ".$_POST['lastname'];
  $skill = explode(",",$_POST['skill']);
  $hoby = explode(",",$_POST['hoby']);
  $password = genRandomString(16);
  $_SESSION["password"] = $password;
  $_SESSION["email"] = $_POST['email'];
  $password = md5($password);

  # check email to not be duplicate
  if(EmailCheck($_POST['email']) === 1) {
    $message_error[] = "*This Email in used. Please enter another one";
    return $message_error;
  }

  # insert in users
  InsertUsers($_POST['email'],$password,$_POST['pnumber'],0);

  # insert in imploye
  InsertImploye(IdUser($_POST['email']),$name,$_POST['gen'],$_POST['birthdate'],$_POST['description'],IdCountry($_POST['country']),IdDegry($_POST['lastdegry']),$_POST['avgdegry'],IdCity($_POST['city']),IdCity($_POST['birthcity']));

  # insert in jobxp
  InsertJobxp($_POST["company"],$_POST["startdate"],$_POST["enddate"],$_POST["reason"],$_POST['email']);

  # insert in skill and hoby
  InsertSkill($skill,$_POST['email']);
  InsertHoby($hoby,$_POST['email']);

  header('Location: login.controller.php');
}

# check email for login
function CheckMailLogin($email)
{
  # check email
  if(EmailCheck($email) !== 1) {
    $message_error = "*Invaild Email or Password";
    return $message_error;
  }
}

# check password for login
function CheckPassLogin($password)
{
  # check pass
  if (PassCheck($password) !== 1) {
    $message_error = "*Invaild Email or Password";
    return $message_error;
  }
}

# set cookie for login
function CookisetLogin()
{
  $randomString = genRandomString(20);
  $expirytime = time()+60*60*24;
  mySetCookie('username',$randomString,$expirytime);
  UpdateSigni($randomString,$_POST['email']);
}

# do all login works
function login()
{
  # check captcha
  if(CheckCaptcha()) {
    $message_error[] = CheckCaptcha();  
  }

  if (empty($_POST["password"]) || empty($_POST["email"])) {
    $message_error = "*Please Fill All Fields";
    return $message_error;
  }

  $password = $_POST["password"];
  $password = md5($password);

  # check email
  if(!empty(CheckMailLogin($_POST["email"]))) {
    AddAttemp();
    $message_error = CheckMailLogin($_POST["email"]);  
    return $message_error;
  }

  # check password
  if(!empty(CheckPassLogin($password))) {
    AddAttemp();
    $message_error = CheckPassLogin($password);  
    return $message_error;
  }

  if (!isset($_POST['rememberme'])) {
    SessionSet($_POST["email"]);
  }
  else {
    SessionSet($_POST["email"]);
    CookisetLogin();
  }

  header('Location: dashboard.controller.php');
}

# edit company
function editCompany()
{
  $email = $_SESSION["login-company"];
  $password = md5($_POST["password"]);

  $source = array("Companyname" => $_POST["companyname"],"Phonenumber" => $_POST["pnumber"],"Password" => $password,"Field" => $_POST["field"]);


  # check Company Name not be duplicate
  if (NameCheckCompanyEdit($source['Companyname'],$email) === 1){
    $message_error[0] = "*This Company Name already taken. Please enter another one";
    return $message_error;
  }

  # update details
  UpdateUser($source["Password"],$source["Phonenumber"],$email);
  # update compnay
  UpdateCompany($email,$source['Companyname']);
  # update field
  UpdateField($source['Companyname'],$source['Field']);

  $message_error[1] = "*Profile was edited";

  return $message_error;
}

# edit imployer
function editImployer()
{
  # get some value from inputs
  $skill = explode(",",$_POST['skill']);
  $hoby = explode(",",$_POST['hoby']);
  $email = $_SESSION["login-imploye"];

  # update users
  UpdateUser(null,$_POST['pnumber'],$email);
  # update imploye
  UpdateImploye($_POST['fullname'],$_POST['gen'],$_POST['birthdate'],$_POST['description'],$_POST['avgdegry'],$_POST['country'],$_POST['lastdegry'],$_POST['city'],$_POST['birthcity'],$email);
  # insert in jobxp
  InsertJobxp($_POST['company'][0],$_POST['startdate'][0],$_POST['enddate'][0],$_POST['reason'][0],$email);

  # delete from skill and hoby
  DeleteSkill($email);
  DeleteHoby($email);

  # insert in skill and hoby
  InsertSkill($skill,$email);
  InsertHoby($hoby,$email);

  $message_error[1] = "*Profile was edited";

  return $message_error;
}